<?php
return [
    'metrika_id' => env('YANDEX_METRIKA_ID'),
    'key' => env('YANDEX_KEY'),
    'secret' => env('YANDEX_SECRET'),
    'received' => env('YANDEX_RECEIVER'),
];