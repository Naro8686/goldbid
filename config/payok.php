<?php
return [
    'sec_key' => env('PAYOK_SEC_KEY'),
    'app_id' => env('PAYOK_APP_ID')
];