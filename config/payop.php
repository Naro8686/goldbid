<?php
return [
    'pub_key' => env('PAYOP_PUB_KEY'),
    'sec_key' => env('PAYOP_SEC_KEY'),
    'app_id' => env('PAYOP_APP_ID'),
    'token' => env('PAYOP_TOKEN'),
];