<?php
return [
    'login' => env('FERMA_LOGIN'),
    'password' => env('FERMA_PASSWORD')
];