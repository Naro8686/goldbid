<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>503 | Технические работы</title>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        img {
            width: 100%;
            height: 100vh;
            object-fit: contain;
        }
    </style>
</head>
<body>
<div class="error-container">
    <img src="{{asset(\App\Models\Setting::first(['image'])->image ?? '')}}" alt="технические работы">
</div>
</body>
</html>