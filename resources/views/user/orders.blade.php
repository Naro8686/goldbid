@extends('layouts.profile')
@section('page')
    <div class="right orders tabContent">
        <div class="title">Мои заказы</div>
        <div id="orders">
            @foreach($orders as $order)
                <div class="item">
                    <div class="block__img">
                        <a href="{{$order->auction_id ? route('auction.index',$order->auction_id) : '#'}}">
                            <img src="{{$order->auction ? asset($order->auction->img_1):asset("site/img/no-image.jpg")}}"
                                 alt="">
                        </a>
                    </div>
                    <div class="block__content">
                        <p><b>Наименование:</b>{{$order->product_name}}</p>
                        <p><b>&#8470; заказа:</b>{{$order->order_num}}</p>
                        <p><b>Цена:</b>{{App\Models\Auction\Auction::moneyFormat($order->price,true,' руб')}}</p>
                        <p><b>Дата:</b>{{$order->created_at->format("Y/m/d")}}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="page">
            {!! $orders->links() !!}
        </div>
    </div>
@endsection

