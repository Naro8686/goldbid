@extends('layouts.profile')
@section('page')
    <div class="right balance tabContent">
        <div class="title">История моих аукционов</div>
        <table class="history">
            <tr>
                <th>Наименование</th>
                <th>Время завершения</th>
                <th>Ставки</th>
                <th>Бонусы</th>
                <th>Результат</th>
            </tr>
            @foreach($bids as $bid)
                <tr>
                    <td>
                        <a style="color: #1B8BCB;"
                           href="{{route('auction.index',$bid->auction_id)}}">{{$bid->auction?$bid->auction->title:"Auction N$bid->auction_id"}}</a>
                    </td>
                    <td style="white-space: nowrap;">{{$bid->auction?$bid->auction->end:$bid->created_at}}</td>
                    <td>{{$bid->bet}}</td>
                    <td>{{$bid->bonus}}</td>
                    <td>{{$bid->reason}}</td>
                </tr>
            @endforeach
        </table>
        <div class="page">
            {!! $bids->links() !!}
        </div>
    </div>
@endsection

