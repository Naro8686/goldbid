@extends('layouts.site')
@push('css')
    <link rel="stylesheet" href="{{asset('site/css/coupon.css')}}">
@endpush
@section('content')
    <div style="padding: 10px 0;" class="main">
        <div class="container order__status">
            <h1>{{$status}}</h1>
            <p>{{$msg}}</p>
            <a class="btn btn-link" href="{{route('site.home')}}">Вернуться на главную страницу </a>
        </div>
    </div>
@endsection
