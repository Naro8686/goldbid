@extends('layouts.site')
@section('name-page')Как это работает@endsection
@push('css')
    <link rel="stylesheet" href="{{asset('site/css/howitwork.css')}}">
@endpush
@section('content')
    <div class="main">
        <div class="container">
            <div class="video-content">
                @foreach($videos as $video)
                    <div class="video-item">
                        {!! $video->iframe() !!}
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container">
            <div class="howitwork">
                @foreach($steps as $key => $step)
                    <div class="step w-100 d-flex @if($key%2) flex-row-reverse @else flex-row @endif">
                        <img class="bg" src="{{asset($step->image)}}" alt="{{$step->alt}}">
                    </div>
                @endforeach
            </div>
            <div class="accordeon">
                <h1>Часто задаваемые вопросы</h1>
                <ul>
                    @foreach($questions as $question)
                        <li>
                            <input type="checkbox" checked>
                            <i></i>
                            <h2>{{$question->title}}</h2>
                            <p>{!! $question->description !!}</p>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        const QUESTIONS_COUNT = parseInt("{{count($questions)}}");
        $(document).ready(function () {
            for (let i = 1; i <= QUESTIONS_COUNT; i++) {
                let delay = 0.25 + (i * 0.25);
                $(`.accordeon ul li:nth-of-type(${i})`).css('animation-delay', `${delay}s`);
            }
        })
    </script>
@endpush
