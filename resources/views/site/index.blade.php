@extends('layouts.site')
@section('slider')
    <div class="slaider" style="background-image: url('{{asset($banner)}}')">
        @foreach($sliders as $slider)
            <div class="slider____item">
                <img src="{{asset($slider->image)}}" alt="{{$slider->alt}}">
            </div>
        @endforeach
    </div>
@endsection
@section('name-page')Аукцион@endsection
@push('css')
    <link href="{{asset('site/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/preloader.css')}}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"
            integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg=="
            crossorigin="anonymous"></script>
@endpush
@section('content')
    @include('site.include.svg_loader')
    <div class="auction container">
        <div id="home_page" class="delete-margin"></div>
    </div>
    @if($ac_content)
        <div class="container aca_cont" style="margin-bottom: 25px">
            <button class="accordion" id="ac_oppener">{{$ac_header}} <span id="toggler_place">
                <i class="fas fa-chevron-down"></i>
            </span></button>
            <div class="panel">
                <p>{!! $ac_content !!}</p>
            </div>
        </div>
    @endif
@endsection
@push('js')
    <script type="text/javascript">
        let hashChanged = false;
        $(document).ready(function () {
            hashChanged = true;
            let page = parseInt(window.location.hash.replace('#', ''));
            if (isNaN(page) || page <= 0) page = 1;
            loadAuctions(page);
        });
        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            hashChanged = true;
            let page = $(this).attr('href').split('page=')[1];
            loadAuctions(page);
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#home_page").offset().top
            }, 2000);
        });
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                let page = window.location.hash.replace('#', '');
                if (hashChanged) hashChanged = false;
                else loadAuctions(page);
            }
        });
        let opened = false;
        if (document.getElementById("ac_oppener"))
            document.getElementById("ac_oppener").addEventListener("click", function () {
                this.classList.toggle("active-ac");
                var panel = this.nextElementSibling;
                if (opened) {
                    document.getElementById('toggler_place').innerHTML = '<i class="fas fa-chevron-down"></i>';
                    panel.style.maxHeight = "0";
                } else {
                    document.getElementById('toggler_place').innerHTML = '<i class="fas fa-chevron-up"></i>';
                    panel.style.maxHeight = "1800px";

                }
                opened = !opened;
            });
    </script>
@endpush
