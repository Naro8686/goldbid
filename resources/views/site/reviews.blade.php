@extends('layouts.site')
@section('name-page')О нас@endsection
@push('css')
    <link rel="stylesheet" href="{{asset('site/css/reviews.css')}}">
@endpush
@section('content')
    <div class="main">
        <div class="container">
            <div class="about">
                <p class="title text-center text-uppercase">О Компании</p>
                <div class="content">{!! $content !!}</div>
            </div>
            <div class="feedback">
                <p class="text-uppercase text-center title">Это Реальные Отзывы О Нас</p>
                <div class="reviews">
                    @foreach($reviews as $key => $review)
                        <div class="review">
                            <div class="review-img">
                                <img src="{{asset($review->avatar())}}" alt="{{$review->alt}}">
                            </div>
                            <div class="review-body">
                                <p>{{$review->title}}</p>
                                <div>{!! $review->description !!}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="send-feedback">
                <p class="text-uppercase text-center title">Оставить отзыв</p>
                <form action="{{route('site.reviews.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="name">
                        <input type="text" placeholder="Ваше имя" name="name" id="name"
                               value="{{old('name')}}">
                        @error('name')
                        <small class="alert alert-danger" role="alert">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="e-mail">
                        <input type="email" placeholder="Е-mail адрес" name="email" id="email"
                               value="{{old('email')}}">
                        @error('email')
                        <small class="alert alert-danger" role="alert">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="msg">
                        <textarea
                                placeholder="Если Вам понравился наш аукцион, или наоборот, есть замечания, напишите об этом.&#x0a;А за развёрнутый и обоснованный отзыв мы начислим Вам Бонусы!"
                                name="message" id="message"
                                cols="30" rows="10">{{old('message')}}</textarea>
                        @error('message')
                        <small class="alert alert-danger" role="alert">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="agree">
                        <label>
                            <input type="checkbox"
                                   @if(old('personal_data')) checked @endif
                                   @error('personal_data') class="fail" @enderror
                                   name="personal_data">
                            На <a href="{{\App\Settings\SiteSetting::dynamicURL('personal-data')}}">обработку</a>
                            персональных данных
                            согласен
                        </label>
                        @if(config('recaptcha.key'))
                            <div class="g-recaptcha"
                                 data-sitekey="{{config('recaptcha.key')}}">
                            </div>
                        @endif
                        @error('g-recaptcha-response')
                        <small class="alert alert-danger" role="alert">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="buttons">
                        <label>
                            <input type="file" name="file" id="uploade-file">
                            <span>Загрузить фото</span>
                        </label>
                        <input type="submit" value="отправить">
                        @error('file')
                        <small class="alert alert-danger" role="alert">{{ $message }}</small>
                        @enderror
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
