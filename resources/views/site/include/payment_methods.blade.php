<div class="payment">
    @foreach(\App\Settings\SiteSetting::paymentGroup() as $group => $payments)
        @switch($group)
            @case("card")
            <div class="payment-type">
                <label class="app-style-input">
                    <input type="radio" name="payment_group" value="{{$group}}">
                    <span></span>
                </label>
                <h4>Банковские карты</h4>
                <ul class="payment-lists">
                    @foreach($payments as $payment)
                        <li data-id="{{$payment['id']}}">
                            <img src="{{asset($payment['img'])}}" alt="{{$payment['value']}}">
                        </li>
                    @endforeach
                </ul>
            </div>
            @break

            @case("yoo-money")
            <div class="payment-type">
                <label class="app-style-input">
                    <input type="radio" name="payment_group" value="{{$group}}">
                    <span></span>
                </label>
                <h4>ЮMoney кошелек</h4>
                <ul class="payment-lists">
                    @foreach($payments as $payment)
                        <li data-id="{{$payment['id']}}">
                            <img src="{{asset($payment['img'])}}" alt="{{$payment['value']}}">
                        </li>
                    @endforeach
                </ul>
            </div>
            @break
            @case("qiwi")
            <div class="payment-type">
                <label class="app-style-input">
                    <input type="radio" name="payment_group" value="{{$group}}">
                    <span></span>
                </label>
                <h4>Qiwi кошелек</h4>
                <ul class="payment-lists">
                    @foreach($payments as $payment)
                        <li data-id="{{$payment['id']}}">
                            <img src="{{asset($payment['img'])}}" alt="{{$payment['value']}}">
                        </li>
                    @endforeach
                </ul>
            </div>
            @break
            @case("perfect-money")
            <div class="payment-type">
                <label class="app-style-input">
                    <input type="radio" name="payment_group" value="{{$group}}">
                    <span></span>
                </label>
                <h4>Perfect Money</h4>
                <ul class="payment-lists">
                    @foreach($payments as $payment)
                        <li data-id="{{$payment['id']}}">
                            <img src="{{asset($payment['img'])}}" alt="{{$payment['value']}}">
                        </li>
                    @endforeach
                </ul>
            </div>
            @break

            @case("tel-operators")
            <div class="payment-type">
                <label class="app-style-input">
                    <input type="radio" name="payment_group" value="{{$group}}">
                    <span></span>
                </label>
                <h4>Сотовые операторы</h4>
                <ul class="payment-lists">
                    @foreach($payments as $payment)
                        <li data-id="{{$payment['id']}}">
                            <img src="{{asset($payment['img'])}}" alt="{{$payment['value']}}">
                        </li>
                    @endforeach
                </ul>
            </div>
            @break
        @endswitch
    @endforeach
</div>