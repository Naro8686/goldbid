@extends('layouts.site')
@section('name-page')Пакеты ставок@endsection
@push('css')
    <link rel="stylesheet" href="{{asset('site/css/coupon.css')}}">
@endpush
@section('content')
    <div class="main">
        <div class="container">
            <p class="title">1. Выберите пакет ставок</p>
            <p>Чем крупнее пакет ставок выберите, тем больше получите Бонусов! Это даст огромное преимущество перед
                другими игроками!</p>
            <div class="blocks mb-100">
                @foreach($packages as $package)
                    <div class="kupon">
                        <label data-id="{{$package->id}}" data-bet="{{$package->bet}}" data-bonus="{{$package->bonus}}"
                               data-price="{{$package->price}}" class="uncheck"></label>
                        <img src="{{asset($package->image)}}" alt="{{$package->alt}}">
                        @if((int)$package->bonus)
                            <div class="kupon-bonus">{{'+'.$package->bonus.' Бонусов'}}</div>
                        @endif
                        <p class="price">{{$package->bet.' Ставок'}} <br> {{$package->price.' руб'}}</p>
                    </div>
                @endforeach
            </div>

            <form name="buy" method="POST" id="coupon"
                  action="{{route('payment.coupon.buy')}}">
                @csrf
                <p class="title">2. Выберите способ оплаты</p>
                @include('site.include.payment_methods')
                @auth
                    <p class="title">3. Заказ </p>
                    <br>
                    <div>
                        <p>Номер Вашего заказа: <b id="order">{{$orderNum}}</b></p>
                        <p>Наименование товара: <b>Пакет ставок <span id="bet">0</span></b></p>
                        <p>Количество бонусов: <b id="bonus">0</b></p>
                        <p>Итоговая стоимость: <b id="price">0</b> <b>руб</b></p>
                    </div>
                    <br><br>
                    @if(auth()->user()->email)
                        <div>
                            <p>
                                Кассовый чек будет предоставлен в электронном виде на адрес электронной почты,
                                указанный в личном кабинете
                            </p>
                        </div>
                    @else
                        <div>
                            <label>
                                Укажите адрес электронной почты
                                <input class="@error('email')is-invalid @enderror" style="width: 200px;height: 30px"
                                       type="email" name="email">
                                @error('email')
                                {{$message}}
                                @enderror
                            </label>
                        </div>
                    @endif
                @endauth
                <input type="hidden" name="coupon_id" id="coupon_id">
                <br><br>
                <div class="text-center">
                    <input class="buy" type="submit" value="Купить пакет ставок">
                </div>
            </form>
        </div>
    </div>
    @push('js')
        <script>
            $('.kupon > .uncheck[data-id]').click(function () {
                let coupon_id = $(this).attr("data-id");
                for (const [key, value] of Object.entries($(this).data())) {
                    $(`#${key}`).text(`${value}`);
                }
                $('[name="coupon_id"]').val(coupon_id);
            });
        </script>
    @endpush
@endsection
