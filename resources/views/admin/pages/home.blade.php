@extends('layouts.admin')
@section('content')

    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Главная страница </h1>
        @include('admin.includes.pages.seo_update')
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Баннер</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.banner.change')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row d-flex flex-column justify-content-center align-items-center">
                        <div class="col-md-6">
                            <div class="thumbnail text-center">
                                <img src="{{asset($banner)}}" class="img-fluid img-thumbnail mb-2" alt="banner" id="imageResult">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input " id="upload" name="banner">
                                    <label class="custom-file-label" for="upload">Выберите файл</label>
                                </div>
                                @error('banner')
                                <small class="form-text text-danger" role="alert">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mt-3">
                            <div class="btn-group btn-block" role="group" aria-label="Basic example">
                                <button type="submit" class="btn btn-success w-75">Сохранить</button>
                                <input type="submit" name="default" value="D" class="btn btn-dark w-25" title="По умолчанию">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Slider</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped text-center" width="100%" cellspacing="0" align="center">
                        <thead>
                        <tr>
                            <th>Картинка</th>
                            <th>Alt</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                <a href="{{route('admin.sliders.create')}}" class="btn btn-sm btn-success float-right">добавить</a>
                            </th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($sliders as $slider)
                            <tr>
                                <td><img class="img-fluid img-thumbnail" src="{{asset($slider->image)}}"
                                         alt="{{$slider->alt}}"
                                         width="50"></td>
                                <td>
                                    <p class="name">{{$slider->alt}}</p>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.sliders.edit',[$slider->id])}}" class="btn btn-info">изменить</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#resourceModal"
                                                data-action="{{route('admin.sliders.destroy',[$slider->id])}}">удалить
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Accordion</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.pages.updateAccordionBehindFooter')}}" method="POST">
                    @method("PUT")
                    @csrf

                    <div class="form-group">
                        <label for="social-meta-keywords" class="col-form-label">Загаловок</label>
                        <input type="text" class="form-control"  name="ac_header" value="{{$ac_header ?? ''}}">
                    </div>

                    <div class="form-group">
                        <label for="body">Содержание</label>
                        <textarea rows="10"
                                  class="form-control col-md-12 ck__textarea @error('ac_content') is-invalid @enderror"
                                  name="ac_content">{{$ac_content ?? ''}}</textarea>
                        @error('ac_content')
                        <div class="valid-feedback">{{$message}}</div>
                        @enderror
                    </div>


                    <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                </form>
            </div>
        </div>
    </div>

@endsection
