@extends('layouts.admin')
@section('content')
    <div class="modal fade" tabindex="1" id="cardModal" role="dialog" aria-labelledby="cardModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="cardModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыт</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Заказы</h1>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                {{--                <div class="row text-center">--}}
                {{--                    <div class="col-md-3">--}}
                {{--                        <span class="m-0 font-weight-bold text-primary">Всего пользователей в базе: <span--}}
                {{--                                class="text-dark"></span></span>--}}

                {{--                    </div>--}}
                {{--                    <div class="col-md-3">--}}
                {{--                        <span class="m-0 font-weight-bold text-primary">Активных: <span--}}
                {{--                                class="text-dark"></span></span>--}}

                {{--                    </div>--}}
                {{--                    <div class="col-md-3">--}}
                {{--                        <span class="m-0 font-weight-bold text-primary">Заблокированных: <span--}}
                {{--                                id="count_banned"--}}
                {{--                                class="text-dark"></span></span>--}}

                {{--                    </div>--}}
                {{--                    <div class="col-md-3">--}}
                {{--                        <span class="m-0 font-weight-bold text-primary">Online: <span--}}
                {{--                                class="text-dark"></span></span>--}}

                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped text-nowrap table-sm text-center" width="100%" cellspacing="0"
                           align="center"
                           id="orders_table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Order N</th>
                            <th>Наименование</th>
                            <th>Сумма</th>
                            <th>Метод оплаты</th>
                            <th>Статус</th>
                            <th>Дата</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            $(function () {
                $('#orders_table').DataTable({
                    aLengthMenu: [
                        [100, -1],
                        [100, "Все"]
                    ],
                    processing: true,
                    serverSide: true,
                    ajax: '{{ route('admin.orders.index') }}',
                    "language": {
                        "url": "{{url('/datatables/lang/Russian.json')}}"
                    },
                    "order": [[0, "desc"]],
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'order_num', name: 'order_num'},
                        {data: 'product_name', name: 'product_name'},
                        {data: 'amount', name: 'amount', searchable: false},
                        {data: 'method', name: 'method'},
                        {data: 'status', name: 'status'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                }).on('page.dt', function () {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 2000);
                });
            });
        </script>
    @endpush
@endsection
