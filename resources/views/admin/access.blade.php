@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Доступы</h1>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form class="form-inline" method="POST" action="{{route('admin.role.store')}}">
                    @csrf
                    <div class="form-group mb-2">
                        <p class="m-0">Добавить роль</p>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="roleName" class="sr-only">Название</label>
                        <input name="name" type="text" class="form-control" id="roleName" placeholder="Название">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Добавить</button>
                </form>
                @error('name') <small class="text-danger">{{$message}}</small> @enderror
                <div class="table-responsive">
                    <table class="table table-striped nowrap" width="100%" cellspacing="0" align="center"
                           id="role_table">
                        <thead>
                        <tr>
                            <th>Роль</th>
                            <th>Страницы</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <th>{{ucfirst($role->name)}}</th>
                                <th>
                                    <ul class="list-group">
                                        @foreach($pages as $page)
                                            @php $exists = $role->adminPages()->where('id',$page->id)->exists() @endphp
                                            <li class="list-group-item d-flex justify-content-between">
                                                <label for="page_{{$page->id}}">{{$page->name}}</label>
                                                <button type='button'
                                                        class='btn btn-sm btn-toggle @if($exists) active @endif'
                                                        id="page_{{$page->id}}"
                                                        data-toggle='button'
                                                        @if($exists)
                                                        aria-pressed='true'
                                                        @else
                                                        aria-pressed='false'
                                                        @endif
                                                        onclick='oNoFF("{{route('admin.settings.access')}}",{role_id:"{{$role->id}}",admin_page_id:"{{$page->id}}"},`PUT`)'>
                                                    <span class='handle'></span>
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </th>
                                <th>
                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#resourceModal"
                                            data-action="{{route('admin.role.destroy',$role->id)}}">
                                        удалить
                                    </button>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
