@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <form action="{{route('admin.video.update',$video->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method("PUT")
            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('admin.pages.howitworks')}}"
                       class="btn btn-light btn-icon-split float-right mb-2">
                            <span class="icon text-gray-600">
                              <i class="fas fa-arrow-left"></i>
                            </span>
                        <span class="text">назад</span>
                    </a>
                </div>
                <div class="col-md-12">
{{--                    <div class="form-group">--}}
{{--                        <a href="{{route('admin.pages.howitworks')}}"--}}
{{--                           class="btn btn-light btn-icon-split float-right mb-2">--}}
{{--                            <span class="icon text-gray-600">--}}
{{--                              <i class="fas fa-arrow-left"></i>--}}
{{--                            </span>--}}
{{--                            <span class="text">назад</span>--}}
{{--                        </a>--}}
{{--                        <input type="text" name="title" value="{{old('title') ?? $video->title}}" class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Заголовок">--}}
{{--                        @error('title')--}}
{{--                        <small class="form-text text-danger" role="alert">{{ $message }}</small>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control @error('link') is-invalid @enderror"
                                   name="link" placeholder="Ссылка"
                                   value="{{old('link') ?? $video->url}}">
                            @error('link')
                            <small class="form-text text-danger" role="alert">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="col">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input @error('file') is-invalid @enderror" id="upload" name="file">
                                <label class="custom-file-label" for="upload">Выберите видео</label>
                                @error('file')
                                <small class="form-text text-danger" role="alert">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mt-3">
                    <button class="btn btn-block btn-outline-success">Сохранить</button>
                </div>
            </div>
        </form>

    </div>
@endsection
