@extends('layouts.admin')
@push('css')
    <style>
        .image-preview, .image-upload .image-preview > div {
            border-radius: inherit !important;
            border-width: 0 !important;
        }

        /*@media only screen and (min-width: 600px) {*/
        /*    select#type {*/
        /*        width: 270px !important;*/
        /*    }*/

        /*    input[type="number"][name^='storage_period'] {*/
        /*        width: 80px !important;*/
        /*    }*/
        /*}*/
    </style>
@endpush
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Настройки сайта</h1>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Параметры</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.settings.site')}}" method="POST" enctype="multipart/form-data">
                    <div class="form-row">
                        @csrf
                        <div class="form-group col-md-6">
                            <label for="email" class="col-form-label">E-mail для показа на сайте </label>
                            <input type="email" class="form-control" id="email"
                                   name="email" value="{{old('email')??$site->email}}">
                            @error('email')
                            <small class="form-text text-danger" role="alert">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone" class="col-form-label">Телефон для показа на сайте </label>
                            <input
                                    type="text" class="form-control" id="phone" name="phone_number"
                                    value="{{old('phone_number')??$site->phone_number}}">
                            @error('phone_number')
                            <small class="form-text text-danger" role="alert">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-6 d-flex flex-column justify-content-center align-items-center">
                            <label for="on_off" class="col-form-label">Включение сайта для пользователей</label>
                            <div class="d-flex align-items-center h-100">
                                <button type="button" id="on_off"
                                        class="btn btn-sm btn-toggle @if($site->site_enabled) active @endif"
                                        data-toggle="button" @if($site->site_enabled) aria-pressed="true"
                                        @else aria-pressed="false" @endif  autocomplete="off"
                                        onclick='oNoFF("{{route('admin.settings.site')}}",{site_enabled:($(this).attr("aria-pressed") === "true" ? 0 : 1),},"POST")'>
                                    <span class="handle"></span>
                                </button>
                            </div>
                        </div>
                        <div class="form-group col-md-6 d-flex justify-content-center flex-column align-items-center">
                            <label for="imageUpload" class="col-form-label">Заставка на сайте</label>
                            <div class="image-upload m-0">
                                <div class="image-edit">
                                    <input type="file" class="imageUpload" id="imageUpload" name="file"
                                           accept=".png, .jpg, .jpeg, .gif">
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="image-preview">
                                    <div class="imagePreview"
                                         @if(!is_null($site->image)) style='background-image: url("{{asset($site->image)}}")' @endif></div>
                                </div>
                            </div>
                            @error('file')
                            <small class="form-text text-danger" role="alert">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row mt-2 mb-3">
                        <div class="table-responsive">
                            <table class="table borderless">
                                <thead>
                                <tr>
                                    <th class="text-center">Тип аукциона</th>
                                    <th class="text-center">Длительность нахождения на сайте</th>
                                    <th class="text-center">Длительность хранения в БД</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($site->storage_period() as $type => $values)
                                    <tr>
                                        <td>
                                            <select id="type" class="form-control" name="storage_period[{{$type}}]"
                                                    readonly="">
                                                <option value="{{$type}}">{{__($type)}}</option>
                                            </select>
                                        </td>
                                        @foreach($values as $key => $value)
                                            <td>
                                                <div class="d-flex">
                                                    <input type="number" class="form-control text-center mr-2"
                                                           name="storage_period[{{$type}}][{{$key}}][date]"
                                                           value="{{old("storage_period.$type.$key.date",$value['date'])}}"
                                                           min="1">
                                                    <select class="form-control text-center"
                                                            name="storage_period[{{$type}}][{{$key}}][format]">
                                                        @foreach(['minutes' => 'мин','hours' => 'час','days' => 'день','months' => 'месяц','years' => 'год'] as $format => $name)
                                                            <option @if(old("storage_period.$type.$key.format",$value['format']) === $format ) selected
                                                                    @endif value="{{$format}}">{{$name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @error('storage_period.*')
                        <small class="form-text text-danger" role="alert">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{{__('Save')}}" class="btn btn-block btn-outline-success">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
