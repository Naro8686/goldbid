<?php

namespace Database\Seeders;

use App\Models\AdminPages;
use Illuminate\Database\Seeder;

class AdminPagesSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'name' => 'Аукционы',
                'icon' => '<i class="fas fa-fw fa-tachometer-alt"></i>',
                'route' => 'admin.dashboard',
                'url' => '/admin',
                'group' => null
            ],
            [
                'name' => 'Каталог',
                'icon' => '<i class="fas fa-fw fa-cog"></i>',
                'route' => 'admin.products.index',
                'url' => '/admin/products',
                'group' => null
            ],
            [
                'name' => 'Пользователи',
                'icon' => '<i class="fas fa-fw fa-user-alt"></i>',
                'route' => 'admin.users.index',
                'url' => '/admin/users',
                'group' => null
            ],
            [
                'name' => 'Боты',
                'icon' => '<i class="fas fa-fw fa-robot"></i>',
                'route' => 'admin.bots.index',
                'url' => '/admin/bots',
                'group' => null
            ],
            [
                'name' => 'Заказы',
                'icon' => '<i class="fas fa-cart-arrow-down"></i>',
                'route' => 'admin.orders.index',
                'url' => '/admin/orders',
                'group' => null
            ],
            [
                'name' => 'Доступы',
                'icon' => null,
                'route' => 'admin.settings.access',
                'url' => '/admin/settings/access',
                'group' => 'settings'
            ],
            [
                'name' => 'Рассылки',
                'icon' => null,
                'route' => 'admin.settings.mailing',
                'url' => '/admin/settings/mailing',
                'group' => 'settings'
            ],
            [
                'name' => 'Настройки Е-майл',
                'icon' => null,
                'route' => 'admin.settings.mail',
                'url' => '/admin/settings/mail',
                'group' => 'settings'
            ],
            [
                'name' => 'Настройки сайта',
                'icon' => null,
                'route' => 'admin.settings.site',
                'url' => '/admin/settings/site',
                'group' => 'settings'
            ],
            [
                'name' => 'Главная',
                'icon' => null,
                'route' => 'admin.pages.home',
                'url' => '/admin/pages/home',
                'group' => 'pages'
            ],
            [
                'name' => 'Как это работает',
                'icon' => null,
                'route' => 'admin.pages.howitworks',
                'url' => '/admin/pages/howitworks',
                'group' => 'pages'
            ],
            [
                'name' => 'О нас',
                'icon' => null,
                'route' => 'admin.pages.reviews',
                'url' => '/admin/pages/reviews',
                'group' => 'pages'
            ],
            [
                'name' => 'Обратная связь',
                'icon' => null,
                'route' => 'admin.pages.feedback',
                'url' => '/admin/pages/feedback',
                'group' => 'pages'
            ],
            [
                'name' => 'Пополнить баланс',
                'icon' => null,
                'route' => 'admin.pages.coupon',
                'url' => '/admin/pages/coupon',
                'group' => 'pages'
            ],
            [
                'name' => 'Оформление заказа',
                'icon' => null,
                'route' => 'admin.pages.order',
                'url' => '/admin/pages/order',
                'group' => 'pages'
            ],
            [
                'name' => 'Подвал сайта',
                'icon' => null,
                'route' => 'admin.pages.footer',
                'url' => '/admin/pages/footer',
                'group' => 'pages'
            ],
        ];
        foreach ($pages as $page) AdminPages::updateOrCreate($page);
    }
}
