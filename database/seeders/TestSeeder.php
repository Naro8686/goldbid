<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Auction\Product;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        if (!is_dir(public_path('site/img/product'))) mkdir(public_path('site/img/product'), 0777, true);
        User::factory()->count(10)->create();
        Product::factory()->count(10)->create();
    }
}
