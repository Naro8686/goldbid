<?php

namespace Database\Seeders;

use App\Models\Balance;
use App\Models\User;
use App\Settings\SiteSetting;
use App\Models\Setting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::create([
            'is_admin' => true,
            'nickname' => 'SuperAdmin',
            'phone' => '70000000000',
            'email' => 'GoldBid24@gmail.com',
            'password' => Hash::make('secret'),
            'email_code' => SiteSetting::emailRandomCode(),
            'email_code_verified' => now(config('app.timezone')),
            'remember_token' => Str::random(10),
        ]);
        $admin->balanceHistory()->create(['reason' => Balance::ADMIN, 'type' => Balance::PLUS, 'bet' => 100, 'bonus' => 100]);
        Setting::firstOrCreate(['phone_number' => '70000000000', 'email' => 'goldbid24@gmail.com']);
    }
}
