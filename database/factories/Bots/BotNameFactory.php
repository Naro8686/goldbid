<?php

namespace Database\Factories\Bots;

use App\Models\Bots\BotName;
use Illuminate\Database\Eloquent\Factories\Factory;

class BotNameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BotName::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->userName,
        ];
    }
}