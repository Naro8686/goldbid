<?php

namespace Database\Factories\Auction;

use App\Models\Auction\Company;
use App\Models\Auction\Category;
use App\Models\Auction\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(15),
            'short_desc' => $this->faker->text(10),
            'desc' => $this->faker->realText(1500),
            'specify' => $this->faker->realText(1500),
            'terms' => $this->faker->realText(1500),
            'exchange' => rand(0, 1),
            'buy_now' => rand(0, 1),
            'top' => rand(0, 1),
            'visibly' => false,
            'start_price' => $this->faker->numberBetween(5, 10),
            'full_price' => $this->faker->numberBetween(100, 10000),
            'bot_shutdown_count' => $this->faker->numberBetween(100, 100) . '-' . $this->faker->numberBetween(1000, 1000),
            'bot_shutdown_price' => $this->faker->numberBetween(1000, 1000) . '-' . $this->faker->numberBetween(10000, 10000),
            'step_time' => $this->faker->numberBetween(10, 20),
            'step_price' => $this->faker->numberBetween(10, 20),
            'to_start' => $this->faker->numberBetween(1, 2),
            'category_id' => function () {
                return Category::factory()->create()->id;
            },
            'company_id' => function () {
                return Company::factory()->create()->id;
            },
            'img_1' => 'site/img/product/' . $this->faker->image('public/site/img/product', 500, 500, null, false),
        ];
    }
}
