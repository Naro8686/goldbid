ClassicEditor
    .create( document.querySelector( '.editor5' ), {

        toolbar: {
        items: [
        'heading',
        '|',
        'bold',
        'italic',
        'link',
        'bulletedList',
        'numberedList',
        '|',
        'outdent',
        'indent',
        '|',
        'blockQuote',
        'insertTable',
        'undo',
        'redo',
        'alignment',
        'fontBackgroundColor',
        'fontSize',
        'highlight',
        'underline'
        ]
    },
        language: 'en',
        table: {
        contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells'
        ]
    },
        licenseKey: '',



    } )
    .then( editor => {
        window.editor = editor;




    } )
    .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: 7rhg4j78yqtq-wam0g59e8dcq' );
        console.error( error );
    } );