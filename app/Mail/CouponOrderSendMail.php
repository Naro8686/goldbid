<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CouponOrderSendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $subject = '';
    /**
     * @var Order
     */
    public $coupon_order;

    public function __construct(Order $order)
    {
        $this->coupon_order = $order;
        $this->subject = "Заказ: Пакет ставок {$order->coupon->bet}, N {$order->order_num}";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coupon_order')
            ->subject($this->subject)
            ->with('coupon_order', $this->coupon_order);
    }
}
