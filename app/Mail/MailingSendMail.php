<?php

namespace App\Mail;

use Exception;
use App\Models\User;
use App\Models\Mailing;
use App\Settings\SiteSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailingSendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Mailing|null
     */
    private $mailing;
    private $user;
    public $data;

    /**
     * MailingSendMail constructor.
     * @param int $type
     * @param array $data
     * @param User|null $user
     * @throws Exception
     */
    public function __construct(int $type, array $data = [], ?User $user = null)
    {
        $this->user = $user;
        $this->data = $data;
        $this->mailing = Mailing::no_ads($type)
            ->where('visibly', true)
            ->first();
        if (is_null($this->mailing)) throw new Exception('error send e-mail');
        if ($type === Mailing::MAIL_CONFIRM && !is_null($user)) {
            $code = SiteSetting::emailRandomCode();
            $this->data['email_code'] = $code;
            $this->data['nickname'] = $this->user->nickname;
            $this->user->update(['email_code' => $code]);
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->mailing->subject;
        $text = $this->mailing->textReplace($this->data);
        return $this->view('emails.mailing', compact('text'))
            ->subject($subject);
    }
}
