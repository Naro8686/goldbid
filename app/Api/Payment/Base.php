<?php

namespace App\Api\Payment;

use App\Models\Order;

abstract class Base
{
    public const URL = null;
    public const NAME = null;
    public const CURRENCY = "RUB";
    public const RETURNED_HTML = false;

    /**
     * @param $orderID
     * @return array|null
     */
    abstract public static function getOrderStatus($orderID): ?array;

    /**
     * @param Order $order
     * @return false|string
     */
    abstract public static function registerOrder(Order $order);

    /**
     * @param $url
     * @param array|string $params
     * @param array $headers
     * @return bool|string
     */
    public static function requestCurl($url, $params = [], array $headers = [])
    {
        $params = is_array($params) ? http_build_query($params) : $params;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param string $type
     * @return string
     */
    public static function returnUrl(string $type): string
    {
        return ($type === Order::AUCTION) ? route('profile.orders') : route('site.checkout.success');
    }
}
