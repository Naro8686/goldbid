<?php


namespace App\Api\Payment;

use App\Models\Order;

class Sberbank extends Base
{
    public const URL = "https://securepayments.sberbank.ru";
    public const NAME = "sberbank";
    public const GROUP = "card";

    public static function getUsername()
    {
        return config('sberbank.username');
    }

    public static function getPassword()
    {
        return config('sberbank.password');
    }

    public static function registerOrder(Order $order)
    {
        $amount = $order->price * 100;
        $response = self::requestCurl(self::URL . "/payment/rest/register.do", [
            'userName' => self::getUsername(),
            'password' => self::getPassword(),
            'orderNumber' => $order->order_num,
            'amount' => $amount,
            'currency' => 643,
            'returnUrl' => self::returnUrl($order->type),
            'failUrl' => route('site.checkout.fail'),
            'dynamicCallbackUrl' => route('api.payment.callback', [self::NAME]),
            'language' => 'ru',
            'sessionTimeoutSecs' => 1200,
        ]);
        if ($response) {
            $response = json_decode($response, true);
            if (isset($response['orderId']) && isset($response['formUrl'])) {
                return $response['formUrl'];
            }
        }
        return false;
    }


    public static function getOrderStatus($orderID): ?array
    {
        $data = null;
        $response = self::requestCurl(self::URL . "/payment/rest/getOrderStatusExtended.do", [
            'userName' => self::getUsername(),
            'password' => self::getPassword(),
            'orderId' => $orderID,
            //'orderNumber' => $orderID,
            'language' => 'ru',
        ]);
        if ($response) {
            $response = json_decode($response, true);
            if (isset($response['orderNumber'])) {
                $data['orderNumber'] = $response['orderNumber'];
                $data['msg'] = $response['actionCodeDescription'];
                $actionCode = $response['actionCode'];
                switch ($actionCode) {
                    case 0:
                        $data['status'] = Order::SUCCESS;
                        break;
                    case -100:
                        $data['status'] = Order::PENDING;
                        break;
                    default:
                        $data['status'] = Order::DECLINED;
                        break;
                }
            }
        }
        return $data;
    }
}
