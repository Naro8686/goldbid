<?php


namespace App\Api\Payment;

use App\Models\Order;

class PayOp extends Base
{
    public const URL = "https://payop.com";
    public const NAME = "payop";
    const GROUP = 'card';

    public static function getPubKey()
    {
        return config('payop.pub_key');
    }

    public static function getSecKey()
    {
        return config('payop.sec_key');
    }

    public static function getAppId()
    {
        return config('payop.app_id');
    }

    public static function getToken()
    {
        return config('payop.token');
    }


    public static function getOrderStatus($orderID): ?array
    {
        return null;
    }

    public static function registerOrder(Order $order)
    {
        $signature = self::generateSign($order->price, $order->order_num, self::getSecKey());
        $response = static::requestCurl(static::URL . "/v1/invoices/create", json_encode([
            'publicKey' => self::getPubKey(),
            'resultUrl' => route('site.checkout.success'),
            'failPath' => route('site.checkout.fail'),
            'language' => 'ru',
            'signature' => $signature,
            'order' => [
                'id' => $order->order_num,
                'amount' => $order->price,
                'currency' => self::CURRENCY,
                'items' => []
            ],
            'payer' => [
                'email' => $order->email()
            ]
        ]), [
            // 'Authorization' => 'Bearer ' . static::TOKEN,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ]);
        if ($response) {
            $response = json_decode($response, true);
            if (!is_null($response) && isset($response['status']) && isset($response['data'])) {
                return "https://payop.com/en/payment/invoice-preprocessing/{$response['data']}";
            }
        }
        return false;
    }

    public static function generateSign($amount, $id, $secretKey): string
    {
        return hash('sha256', implode(':', [$amount, self::CURRENCY, $id, $secretKey]));
    }
}
