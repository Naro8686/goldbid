<?php

namespace App\Api\Payment;

use App\Models\Order;

class Pay
{
    /**
     * @var Order
     */
    protected $order;
    /**
     * @var false|string
     */
    protected $redirect = false;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function register(): Pay
    {
        $order = $this->order;
        switch ($order->method) {
            case "card":
                $class = Sberbank::class;
                break;
            default:
                $class = PayOk::class;
                break;
        }
        $this->redirect = $class::registerOrder($order);
        return $this;
    }

    public function getRedirectUrl()
    {
        return $this->redirect;
    }
}