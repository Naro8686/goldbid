<?php


namespace App\Api\Payment;

use App\Models\Order;
use App\Settings\SiteSetting;

class PayOk extends Base
{
    public const URL = "https://payok.io";
    public const NAME = "payok";
    public const RETURNED_HTML = true;

    public static function getSecKey()
    {
        return config('payok.sec_key');
    }

    public static function getAppId()
    {
        return config('payok.app_id');
    }


    public static function getOrderStatus($orderID): ?array
    {
        return null;
    }

    public static function generateSign($amount, $payment, $desc = ""): string
    {
        return md5(implode('|', [$amount, $payment, self::getAppId(), self::CURRENCY, $desc, self::getSecKey()]));
    }

    public static function registerOrder(Order $order): string
    {
        $amount = $order->price;
        $payment = $order->id;
        $desc = $order->product_name;
        $email = $order->email();
        $signature = self::generateSign($amount, $payment, $desc);
        $method = self::method($order->method);
        $params = [
            'amount' => $amount,
            'payment' => $payment,
            'shop' => self::getAppId(),
            'desc' => $desc,
            'currency' => self::CURRENCY,
            'sign' => $signature,
            'email' => $email,
            'method' => $method,
            'orderNumber' => $order->order_num
        ];
        $query = http_build_query($params);
        return self::URL . "/pay?$query";
    }

    /**
     * @param string $method
     * @return string|null
     */
    public static function method(string $method = "card"): ?string
    {
        return SiteSetting::paymentGroup($method)->pluck(['short_name'])->first();
    }
}
