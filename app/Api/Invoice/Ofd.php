<?php

namespace App\Api\Invoice;

use App\Models\Order;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Ofd
{
    public $domain = 'ferma.ofd.ru';
    protected $inn = '235300093105';
    protected $login;
    protected $password;

    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    protected function token()
    {
        if (!Cache::has('ofdToken')) {
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post("https://ferma.ofd.ru/api/Authorization/CreateAuthToken", [
                'Login' => $this->login,
                'Password' => $this->password,
            ]);

            if (!$response->serverError() && $response->status() === 200) {
                $obj = $response->object();
                $result = $obj->Data ?? $obj;
                Cache::put('ofdToken', $result->AuthToken, Carbon::parse($result->ExpirationDateUtc, config('app.timezone')));
            }
        }
        return Cache::get('ofdToken');
    }

    public function generateReceipt(Order $order)
    {
        $receiptId = null;
        $user = $order->user;
        $token = $this->token();
        $price = number_format($order->price, 2, '.', '');
        $params = [
            "Request" => [
                "Inn" => $this->inn,
                "Type" => "Income",
                "InvoiceId" => $order->order_num,
                "CallbackUrl" => route('api.ofd.callback'),
                "CustomerReceipt" => [
                    "TaxationSystem" => "SimpleIn",
                    "Email" => $user->email,
                    "Phone" => "+$user->phone",
                    "AutomaticDeviceNumber" => null,
                    "PaymentType" => 1,
                    "PaymentAgentInfo" => null,
                    "CorrectionInfo" => null,
                    "Items" => [
                        [
                            "Label" => $order->product_name,
                            "Price" => $price,
                            "Quantity" => 1.0,
                            "Amount" => $price,
                            "Vat" => "VatNo",
                            "MarkingCode" => null,
                            "PaymentMethod" => 0,
                        ]
                    ],
                    "PaymentItems" => null,
                    "CustomUserProperty" => null
                ]
            ]
        ];
        $response = Http::post("https://$this->domain/api/kkt/cloud/receipt?AuthToken=$token", $params);
        if (!$response->serverError() && $response->status() === 200) {
            $obj = $response->object();
            $data = $obj->Data;
            $receiptId = $data->ReceiptId;
        }
        return $receiptId;
    }
}