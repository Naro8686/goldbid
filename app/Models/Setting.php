<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string|null $phone_number
 * @property string|null $email
 * @property bool $site_enabled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSiteEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereStoragePeriodMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $image
 * @property array|null $storage_period
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereStoragePeriod($value)
 */
class Setting extends Model
{
    protected $fillable = ['phone_number', 'email', 'site_enabled', 'storage_period', 'image'];
    protected $casts = [
        'site_enabled' => 'boolean',
        'storage_period' => 'array'
    ];
    protected $attributes = [
        'storage_period' => self::STORAGE_PERIOD_JSON
    ];

    public const STORAGE_PERIOD_JSON = '{"no_bid":{"on_site":{"date":null,"format":null},"on_db":{"date":null,"format":null}},"has_error":{"on_site":{"date":null,"format":null},"on_db":{"date":null,"format":null}},"end_only_bot":{"on_site":{"date":null,"format":null},"on_db":{"date":null,"format":null}},"end":{"on_site":{"date":null,"format":null},"on_db":{"date":null,"format":null}}}';


    public function storage_period(): Collection
    {
        $defaults = json_decode(self::STORAGE_PERIOD_JSON, true);

        $data = $this->storage_period ?? $defaults;
        $temp_arr = [];
        foreach (collect($defaults)->keys() as $type) {
            foreach (collect($defaults[$type])->keys() as $key)
                $temp_arr[$type][$key] = $data[$type][$key];
        }
        return collect($temp_arr);
    }
}
