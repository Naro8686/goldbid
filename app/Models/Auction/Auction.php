<?php

namespace App\Models\Auction;

use App\Jobs\AutoBidJob;
use App\Jobs\BotBidJob;
use App\Models\Balance;
use App\Models\Order;
use App\Models\Setting;
use Cache;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\Models\Bots\AuctionBot;
use Illuminate\Support\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Throwable;
use Log;

/**
 * App\Models\Auction\Auction
 *
 * @property int $id
 * @property string $title
 * @property string|null $short_desc
 * @property string|null $desc
 * @property string|null $specify
 * @property string|null $terms
 * @property string|null $img_1
 * @property string|null $img_2
 * @property string|null $img_3
 * @property string|null $img_4
 * @property string|null $alt_1
 * @property string|null $alt_2
 * @property string|null $alt_3
 * @property string|null $alt_4
 * @property int $start_price
 * @property int $full_price
 * @property string|null $bot_shutdown_count
 * @property string|null $bot_shutdown_price
 * @property int $bid_seconds
 * @property Carbon|null $step_time
 * @property int $step_price
 * @property Carbon $start
 * @property Carbon|null $end
 * @property bool $exchange
 * @property bool $buy_now
 * @property bool $top
 * @property bool $active
 * @property int $status
 * @property array|null $winner
 * @property int|null $product_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|AutoBid[] $autoBid
 * @property-read int|null $auto_bid_count
 * @property-read \Illuminate\Database\Eloquent\Collection|History[] $bid
 * @property-read int|null $bid_count
 * @property-read Product|null $product
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $userFavorites
 * @property-read int|null $user_favorites_count
 * @method static Builder|Auction newModelQuery()
 * @method static Builder|Auction newQuery()
 * @method static Builder|Auction query()
 * @method static Builder|Auction whereActive($value)
 * @method static Builder|Auction whereAlt1($value)
 * @method static Builder|Auction whereAlt2($value)
 * @method static Builder|Auction whereAlt3($value)
 * @method static Builder|Auction whereAlt4($value)
 * @method static Builder|Auction whereBidSeconds($value)
 * @method static Builder|Auction whereBotShutdownCount($value)
 * @method static Builder|Auction whereBotShutdownPrice($value)
 * @method static Builder|Auction whereBuyNow($value)
 * @method static Builder|Auction whereCreatedAt($value)
 * @method static Builder|Auction whereDesc($value)
 * @method static Builder|Auction whereEnd($value)
 * @method static Builder|Auction whereExchange($value)
 * @method static Builder|Auction whereFullPrice($value)
 * @method static Builder|Auction whereId($value)
 * @method static Builder|Auction whereImg1($value)
 * @method static Builder|Auction whereImg2($value)
 * @method static Builder|Auction whereImg3($value)
 * @method static Builder|Auction whereImg4($value)
 * @method static Builder|Auction whereProductId($value)
 * @method static Builder|Auction whereShortDesc($value)
 * @method static Builder|Auction whereSpecify($value)
 * @method static Builder|Auction whereStart($value)
 * @method static Builder|Auction whereStartPrice($value)
 * @method static Builder|Auction whereStatus($value)
 * @method static Builder|Auction whereStepPrice($value)
 * @method static Builder|Auction whereStepTime($value)
 * @method static Builder|Auction whereTerms($value)
 * @method static Builder|Auction whereTitle($value)
 * @method static Builder|Auction whereTop($value)
 * @method static Builder|Auction whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|AuctionBot[] $bots
 * @property-read int|null $bots_count
 * @property-read Order $userOrder
 * @property Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Balance[] $betUsers
 * @property-read int|null $bet_users_count
 * @method static \Illuminate\Database\Query\Builder|Auction onlyTrashed()
 * @method static Builder|Auction whereDeletedAt($value)
 * @method static Builder|Auction whereWinner($value)
 * @method static \Illuminate\Database\Query\Builder|Auction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Auction withoutTrashed()
 */
class Auction extends Model
{
    use SoftDeletes;

    const BET_RUB = 10;

    public const STATUS_PENDING = 1;
    public const STATUS_ACTIVE = 2;
    public const STATUS_FINISHED = 3;
    public const STATUS_ERROR = 4;
    protected $fillable = [
        'title', 'short_desc', 'desc', 'specify', 'terms',
        'img_1', 'img_2', 'img_3', 'img_4',
        'alt_1', 'alt_2', 'alt_3', 'alt_4',
        'start_price', 'full_price',
        'bot_shutdown_count', 'bot_shutdown_price',
        'step_time', 'step_price', 'start', 'end', 'exchange', 'buy_now',
        'top', 'active', 'product_id', 'status', 'bid_seconds', 'winner'
    ];
    protected $dates = ['start', 'end', 'step_time'];
    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
        'status' => 'integer',
        'start_price' => 'integer',
        //'full_price' => 'integer',
        'step_price' => 'integer',
        'bid_seconds' => 'integer',
        'step_time' => 'datetime',
        'exchange' => 'boolean',
        'buy_now' => 'boolean',
        'top' => 'boolean',
        'active' => 'boolean',
        'winner' => 'array',
    ];
    public static $selectAuctionFillable = [
        'auctions.id', 'auctions.title', 'auctions.img_1', 'auctions.alt_1',
        'auctions.start', 'auctions.end', 'auctions.active', 'auctions.status', 'auctions.winner'
    ];

    /**
     * @return string
     */
    public function cacheKey(): string
    {
        return sprintf(
            "%s/%s-%s",
            $this->getTable(),
            $this->getKey(),
            $this->updated_at->timestamp
        );
    }


    /**
     * @return bool
     */
    public function jobExists(): bool
    {
        return $this->whereHas('bots', function ($query) {
            $query->where('auction_bots.auction_id', '=', $this->id)->where('auction_bots.status', '=', AuctionBot::WORKED);
        })->orWhereHas('autoBid', function ($query) {
            $query->where('auto_bids.auction_id', '=', $this->id)->where('auto_bids.status', '=', AutoBid::WORKED);
        })->exists();
    }


    public function queues()
    {
        try {
            $autoBetByBots = new Collection();
            if (!$this->stopBotOne() && $botOne = $this->botNum(1)) {
                $autoBetByBots->put(1, $botOne);
            }
            if (!$this->stopBotTwoThree()) {
                $botTwo = $this->botNum(2);
                $botThree = $this->botNum(3);
                if (!is_null($botTwo)) {
                    if (is_null($botThree)) $autoBetByBots->put(2, $botTwo);
                    else {
                        if (($botTwo->num_moves_other_bot + $botTwo->num_moves) > 0) $autoBetByBots->put(2, $botTwo);
                        else if ($botThree->num_moves <= 0) $autoBetByBots->put(2, $botTwo);
                    }
                }
                if (!is_null($botThree)) {
                    if (is_null($botTwo)) $autoBetByBots->put(3, $botThree);
                    else if ($botTwo->num_moves <= 0 || ($botThree->num_moves <= 0 && $botThree->num_moves_other_bot > 0)) $autoBetByBots->put(3, $botThree);
                }
            }
            $autoBetByUsers = $this->autoBid()->whereHas('user', function ($query) {
                $query->where([
                    ['users.has_ban', '=', false],
                    [\DB::raw('(users.bet + users.bonus)'), '>', 0],
                ]);
            })->where('auto_bids.count', '>', 0)
                ->orderBy('auto_bids.bid_time')
                ->orderBy('auto_bids.id');
            $autoBetByUsers->each(function ($autoBet) {
                if ($autoBet->user->auctionOrder()
                    ->where('orders.auction_id', '=', $autoBet->auction_id)
                    ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
                    ->exists()) {
                    $autoBet->delete();
                }
            });

            $job = Cache::remember("auction.queues.$this->id", $this->bid_seconds, function () use ($autoBetByBots, $autoBetByUsers) {
                $job = new Collection();
                $userIDs = $autoBetByUsers->pluck('user_id');
                $botNums = $autoBetByBots->keys();
                $queues = $this->history()
                    ->whereIn('user_id', $userIDs)
                    ->orWhereIn('bot_num', $botNums)
                    ->groupBy('bot_num')
                    ->groupBy('user_id')
                    ->orderBy('id')
                    ->get()->map(function ($queue) use ($autoBetByBots) {
                        if ($queue->is_bot) $autoBetByBots->offsetUnset($queue->bot_num);
//                else {
//                    $queue->created_at = $queue->auction()->autoBid()->where('user_id', $queue->user_id)->first(['bid_time'])->bid_time;
//                }
                        return $queue;
                    });
                $autoBetByBots->map(function ($bot, $botNum) use ($queues) {
                    $id = $queues->isNotEmpty() ? ($queues->last()->id + 1) : 1;
                    $queues->push((new History([
                        "id" => $id,
                        "is_bot" => true,
                        "bot_num" => $botNum,
                        "user_id" => null,
                        "nickname" => $bot->name,
                        "created_at" => now(config('app.timezone'))
                    ])));
                });

                if ($queues->isNotEmpty()) {
                    $lastBids = $this->history()
                        ->whereIn('user_id', $userIDs)
                        ->orWhereIn('bot_num', $botNums)
                        ->orderByDesc('id')
                        ->take(1)
                        ->first();
                    //dump($queues->sortByDesc("created_at"));
                    $current = $queues->first();

                    if (!is_null($lastBids)) {
                        $type = $lastBids->is_bot ? "bot_num" : "user_id";
                        $key = array_search($lastBids->{$type}, array_column($queues->toArray(), $type));
                        $next = current(array_slice($queues->toArray(), $key + 1, 1, true));
                        $current = (($next === false) ? $queues->first() : $queues->firstWhere('id', '=', $next['id']));
                    }

                    if (!is_null($current)) {
                        if ($current->is_bot) {
                            $model = $this->botNum($current->bot_num);
                            $nickname = $model->name;
                            $job->push([
                                'model' => $model,
                                'nickname' => $nickname,
                                'className' => BotBidJob::class,
                            ]);
                        } else {
                            /** @var AutoBid $model */
                            $model = $autoBetByUsers->where('user_id', $current->user_id)->first();
                            $nickname = $model->user->nickname;
                            $job->push([
                                'model' => $model,
                                'nickname' => $nickname,
                                'className' => AutoBidJob::class,
                            ]);
                        }
                    }
                }
                return $job;
            });
            return $job->firstWhere('nickname', '<>', $this->winner()->nickname);
        } catch (Throwable|Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }

    /**
     * @param int $num
     * @return int
     */
    public function botCountBet(int $num = 1): int
    {
        $count = 0;
        try {
            $count = $this->noRates() ? 0 : $this->bid()->where('bot_num', '=', $num)->count();
        } catch (Exception|Throwable $throwable) {
        }
        return $count;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function type($isItTrue = null)
    {
        $type = Step::BET;
        if ($this->buy_now) $type = Step::PRODUCT;
        elseif ($this->exchange) $type = Step::MONEY;
        return is_null($isItTrue)
            ? $type
            : ($type === $isItTrue);
    }

    public function userFavorites()
    {
        return $this->belongsToMany(User::class, 'favorites', 'auction_id', 'user_id');
    }

    public function autoBid()
    {
        return $this->hasMany(AutoBid::class);
    }

    public function betUsers()
    {
        return $this->hasMany(Balance::class);
    }

    public function bots()
    {
        return $this->hasMany(AuctionBot::class);
    }

    /**
     * @param int $num
     * @param array|string[] $select
     * @return AuctionBot|Builder|Model|HasMany|object|null
     */
    public function botNum(int $num, array $select = ['*'])
    {
        return $this->bots()->whereHas('bot', function ($query) use ($num) {
            $query->where('number', '=', $num);
        })->first($select);
    }

    public function stopBotOne(): bool
    {
        $stopBotOne = $count = (int)$this->bot_shutdown_count;
        try {
            if (!$this->noRates() && !is_null($this->botNum(1))) {
                $isFirst = $this->bid()->orderBy('id')->take(1)->first();
                if (!is_null($isFirst) && $isFirst->bot_num === 1)
                    $count = $this->botCountBet();
            }
        } catch (Exception|Throwable $throwable) {
        }
        return ($count >= $stopBotOne);
    }

    public function stopBotTwoThree(): bool
    {
        $stopBotTwoThree = $sumBids = (int)$this->bot_shutdown_price;
        try {
            if (!$this->noRates() && $this->bid()->take(1)->exists() && (!is_null($this->botNum(2)) || !is_null($this->botNum(3)))) {
                $sumBids = (int)($this->bid()->where('is_bot', false)->sum('bet') * self::BET_RUB);
            }
        } catch (Exception|Throwable $throwable) {
        }

        return ($sumBids >= $stopBotTwoThree);
    }

    public function shutdownBots(): bool
    {
        return ($this->stopBotOne() && $this->stopBotTwoThree());
    }

    public function achieved(): bool
    {
        $stopBotTwoThree = (int)$this->bot_shutdown_price;
        try {
            $sumBids = ($this->bid()->where('is_bot', false)->sum('bet') * self::BET_RUB);
        } catch (Throwable $exception) {
            $sumBids = 0;
            Log::error("achieved:{$exception->getMessage()}");
        }
        return ($sumBids >= $stopBotTwoThree);
    }

    public function bid()
    {
        return $this->history();
    }

    public function hasHistory(): bool
    {
        $hasTable = false;
        try {
            $tableName = $this->getLogTableName();
            $hasTable = Schema::hasTable($tableName);
        } catch (Throwable $exception) {
            Log::error("hasHistory:{$exception->getMessage()}");
        }
        return $hasTable;
    }


    public function userOrder()
    {
        return $this->hasOne(Order::class);
    }

    public static function moneyFormat($price, $optional = false, $text = ' ₽'): string
    {
        $text = $optional ? $text : '';
        $format = number_format($price, 1, ',', ' ');
        return $format . $text;
    }

    /**
     * @return bool
     */
    public function finished(): bool
    {
        return !is_null($this->step_time) && Carbon::now(config('app.timezone'))->diff($this->step_time)->invert;
    }

    public function bidTable(): string
    {
        $tr = "";
        try {
            if (!$this->noRates()) foreach ($this->bid()->orderBy('id', 'desc')->take(5)->get() as $bid) {
                $price = self::moneyFormat($bid->price, true);
                $time = $bid->created_at->format('H:i:s') ?? $bid->created_at;
                $tr .= "<tr>
                      <td>$price</td>
                      <td>$bid->nickname</td>
                      <td>$time</td>
                    </tr>";
            }
        } catch (Throwable $exception) {
            Log::error($exception->getMessage());
        }
        return $tr;
    }

    public function winner()
    {
        try {
            return $this->isFinish()
                ? (!is_null($this->winner) ? new History($this->winner) : new History())
                : $this->history()->orderByDesc('id')->take(1)->firstOrNew();
        } catch (Exception|Throwable $throwable) {
            return new History();
        }
    }

    public function getLogTableName(): string
    {
        return History::PREFIX . $this->id;
    }


    /**
     * @return History
     */
    public function history(): History
    {
        $history = new History;
        try {
            $history->bind($this->getConnectionName(), $this->getLogTableName());
        } catch (Exception|Throwable $throwable) {
        }
        return $history;
    }


    public function status(): string
    {
        $text = '';
        switch ($this->status) {
            case self::STATUS_PENDING:
                $text = 'Скоро начало';
                break;
            case self::STATUS_ACTIVE:
                $text = 'Активный';
                break;
            case self::STATUS_ERROR:
                $text = 'Ошибка';
                break;
            case self::STATUS_FINISHED:
                $text = !is_null($this->winner)
                    ? "Победил " . (($this->winner()->is_bot) ? 'бот' : 'игрок')
                    : "Не состоялся";
                break;
        }
        return $text;
    }

    public function price()
    {
        return is_null($this->winner()->price)
            ? $this->start_price
            : $this->winner()->price;
    }

    /**
     * @return string
     */
    public function new_price()
    {
        return number_format($this->price() + $this->step_price(), 2, '.', '');
    }

//    public function countBid(): float
//    {
//        $price = $this->bid()->doesntExist()
//            ? $this->start_price
//            : $this->step_price();
//        return round($price * Auction::BET_RUB);
//    }

    public function step_price()
    {
        return ($this->step_price / 100);
    }

    public function start_price(): string
    {
        return self::moneyFormat($this->start_price);
    }

    public function full_price($user_id = null)
    {
        try {
            if ($this->full_price <= 0) return 0;
            $user_bet = (!is_null($user_id) && !$this->noRates()) ? $this->bid()->where('user_id', $user_id)->sum('bet') : 0;
            $new_price = ((float)$this->full_price - ($user_bet * self::BET_RUB));
            return ($new_price <= 0) ? 1 : $new_price;
        } catch (Exception|Throwable $throwable) {
            return $this->full_price;
        }
    }

    public static function info(): Collection
    {
        return collect([
            'auction_count' => self::count(),
            'active_count' => self::where('active', true)->count()
        ]);
    }

    public function auctionCard()
    {
        return $this->history()
            ->where('is_bot', false)
            ->groupBy('user_id')
            ->get([
                'user_id',
                DB::raw('SUM(bet) AS bet'),
                DB::raw('SUM(bonus) AS bonus')
            ]);
    }

    public function start()
    {
        return !Carbon::now(config('app.timezone'))->diff($this->start)->invert
            ? $this->start->diffInSeconds()
            : 0;
    }

    public function noRates(): bool
    {
        return $this->isFinish() && is_null($this->winner);
    }

    public function exchangeBetBonus($user_id = null): array
    {
        $bet = $bonus = 0;
        if (!$this->noRates() && $this->status === self::STATUS_FINISHED && !is_null($user_id)) {
            $bet = (int)round($this->full_price() / self::BET_RUB);
            $bonus = (int)round($bet / 2);
        }
        return ['bet' => $bet, 'bonus' => $bonus];
    }

    public function step_time()
    {
        return !Carbon::now(config('app.timezone'))->diff($this->step_time)->invert
            ? $this->step_time->diffInSeconds()
            : 0;
    }

    public function images(): array
    {
        $images = [];
        if (!is_null($this->img_1)) $images[] = ['img' => $this->img_1, 'alt' => $this->alt_1];
        if (!is_null($this->img_2)) $images[] = ['img' => $this->img_2, 'alt' => $this->alt_2];
        if (!is_null($this->img_3)) $images[] = ['img' => $this->img_3, 'alt' => $this->alt_3];
        if (!is_null($this->img_4)) $images[] = ['img' => $this->img_4, 'alt' => $this->alt_4];
        return $images;
    }

    public function transformAuction($userID = null): Collection
    {
        $winner = $this->winner();
        return collect([
            'id' => $this->id,
            'top' => $this->top,
            'favorite' => (bool)$this->user_favorites_count,
            'autoBid' => $this->auto_bid_count,
            'short_desc' => $this->short_desc,
            'status' => $this->status,
            'images' => $this->images(),
            'title' => $this->title,
            'step_price' => $this->step_price(),
            'step_price_info' => $this->step_price,
            'start_price' => $this->start_price(),
            'exchange' => $this->exchange,
            'buy_now' => $this->buy_now,
            'full_price' => self::moneyFormat($this->full_price($userID)),
            'exchangeBetBonus' => $this->exchangeBetBonus($userID),
            'bid_seconds' => $this->bid_seconds,
            'step_time' => $this->step_time ? $this->step_time() : null,
            'start' => $this->start(),
            'winner' => $winner->nickname,
            'my_win' => !is_null($userID) && $this->isFinish() && $winner->user_id === $userID,
            'error' => !is_null($userID) && $this->status === Auction::STATUS_ERROR && $winner->user_id === $userID,
            'ordered' => (bool)$this->user_order_count,
            'price' => self::moneyFormat($this->price()),
            'end' => $this->end ? $this->end->format('Y-m-d H:i:s') : null,
        ]);
    }

    public function isFinish(): bool
    {
        return ($this->status === Auction::STATUS_FINISHED || $this->status === Auction::STATUS_ERROR);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public static function auctionsForHomePageQuery(array $where = [['active', true]])
    {
        $userID = Auth::id();
        return self::where($where)
            ->withCount([
                'userFavorites' => function ($favorite) use ($userID) {
                    return $favorite->where('users.id', $userID);
                }, 'autoBid' => function ($autoBid) use ($userID) {
                    return $autoBid->where('user_id', '=', $userID)->select(['count']);
                }, 'userOrder' => function ($order) use ($userID) {
                    return $order->where([
                        ['user_id', '=', $userID],
                        ['type', '=', Order::AUCTION],
                        ['status', '=', Order::SUCCESS]
                    ]);
                }]);
    }

    public static function auctionsForHomePage(int $paginate = 25): LengthAwarePaginator
    {
        $userID = Auth::check() ? Auth::id() : "NULL";
        $itemsPaginated = self::auctionsForHomePageQuery()
            ->orderByRaw("IF((auctions.winner IS NOT NULL) AND (auctions.status <> " . Auction::STATUS_ERROR . ") AND (auctions.winner->>'$.user_id' IS NOT NULL) AND (user_order_count = 0) AND (auctions.winner->>'$.user_id' = $userID),1,0) DESC")
            ->orderByDesc('top')
            ->orderByDesc('user_favorites_count')
            ->orderByRaw('(CASE WHEN `auctions`.`status` = ' . Auction::STATUS_ACTIVE . ' THEN `auctions`.`status` END) DESC,
                              (CASE WHEN `auctions`.`status` = ' . Auction::STATUS_PENDING . ' THEN `auctions`.`status` END) DESC,
                              (CASE WHEN `auctions`.`status` = ' . Auction::STATUS_FINISHED . ' THEN `auctions`.`end` END) DESC,
                              (CASE WHEN `auctions`.`status` = ' . Auction::STATUS_ERROR . ' THEN `auctions`.`end` END) DESC')
            ->orderByRaw('(CASE WHEN `auctions`.`status` = ' . Auction::STATUS_PENDING . ' THEN `auctions`.`start` END) ASC,
                              (CASE WHEN `auctions`.`status` = ' . Auction::STATUS_ACTIVE . ' THEN `auctions`.`start` END) DESC')
            ->paginate($paginate);
        $itemsTransformed = collect($itemsPaginated->items())->map(function (Auction $auction) use ($userID) {
            try {
                $transformAuction = $auction->transformAuction($userID)->toArray();
            } catch (Exception|Throwable $throwable) {
                $transformAuction = [];
            }
            return $transformAuction;
        });
        return new LengthAwarePaginator(
            $itemsTransformed,
            $itemsPaginated->total(),
            $itemsPaginated->perPage(),
            $itemsPaginated->currentPage(), [
            'path' => request()->url(),
            'query' => ['page' => $itemsPaginated->currentPage()]
        ]);
    }

    public static function auctionPage($id, $where = []): Collection
    {
        $userID = Auth::id();
        $auction = self::auctionsForHomePageQuery($where)->findOrFail($id);
        $noRate = $auction->noRates();
        $data = collect($auction);
        $data['bet'] = $data['bonus'] = 0;
        try {
            $data = $auction->transformAuction($userID);
            $data['bids'] = $noRate ? [] : $auction->bid()->orderByDesc('id')
                ->take(5)
                ->get(['nickname', 'price', 'created_at'])->transform(function ($bid) {
                    $res['nickname'] = $bid->nickname;
                    $res['created_at'] = $bid->created_at->format('H:i:s');
                    $res['price'] = self::moneyFormat($bid->price, true);
                    return $res;
                });
            if (!is_null($userID) && !$noRate) {
                $bid = $auction->bid()->where('user_id', $userID);
                $data['bet'] = $bid->sum('bet');
                $data['bonus'] = $bid->sum('bonus');
            }

        } catch (Exception|Throwable $throwable) {
        }
        $data['desc'] = $auction->desc;
        $data['specify'] = $auction->specify;
        $data['terms'] = $auction->terms;

        return $data;
    }

    /**
     * @return mixed
     */
    public function bidDataForUser(): array
    {
        $data['user'] = $data['auction'] = [];
        try {
            if (!$this->noRates()) {
                $last = $this->winner();
                if (!is_null($last->user_id) && $user = User::find($last->user_id)) {
                    $balance = $user->balance();
                    $bid = $this->bid()->where('user_id', $user->id);
                    $autoBid = $user->autoBid()->where('auto_bids.auction_id', $this->id)->first(['auto_bids.count']);
                    $data['user']['id'] = $user->id;
                    $data['user']['bet'] = $balance->bet;
                    $data['user']['bonus'] = $balance->bonus;
                    $data['user']['auction_bet'] = $bid->sum('bet');
                    $data['user']['auction_bonus'] = $bid->sum('bonus');
                    $data['user']['full_price'] = self::moneyFormat($this->full_price($user->id), true);
                    $data['user']['auto_bid'] = !is_null($autoBid) ? $autoBid->count : null;
                }
                if (!is_null($last->nickname)) {
                    $data['auction']['id'] = $this->id;
                    $data['auction']['step_time'] = $this->step_time();
                    $data['auction']['nickname'] = $last->nickname;
                    $data['auction']['price'] = self::moneyFormat($last->price, true);
                }
                $data['auction']['tr'] = $this->bidTable();
            }
        } catch (Exception|Throwable $throwable) {
            Log::error('bidDataForUser - ' . $throwable->getMessage());
        }
        return $data;
    }

    public static function data(): Collection
    {
        $all = collect();
        Auction::select(self::$selectAuctionFillable)
            ->orderBy("id")
            ->chunk(100, function ($auctions) use ($all) {
                foreach ($auctions as $auction) {
                    /** @var Auction $auction */
                    try {
                        $all->push([
                            'id' => $auction->id,
                            'title' => $auction->title,
                            'img_1' => $auction->img_1,
                            'alt_1' => $auction->alt_1,
                            'status' => $auction->status(),
                            'active' => $auction->active ? 'да' : 'нет',
                            'bet' => (int)$auction->history()->where('is_bot', false)->sum('bet'),
                            'bonus' => (int)$auction->history()->where('is_bot', false)->sum('bonus'),
                            'bot' => $auction->history()->where('is_bot', true)->count('id'),
                            'start' => $auction->start->format('Y-m-d H:i:s'),
                            'end' => $auction->end ? $auction->end->format('Y-m-d H:i:s') : null,
                        ]);
                    } catch (Throwable $exception) {
//                        Log::error("Auction:data:{$exception->getMessage()}");
                    }
                }

            });
        return $all;
    }

    public function storagePeriodCheck(): object
    {
        $exists = false;
        $isActive = $this->active;
        try {
            if (!is_null($this->end)) {
                $current = now($this->end->timezoneName);
                $setting = Setting::whereNotNull('storage_period')->first();
                if (!is_null($setting)) {
                    $storage_period = $setting->storage_period();
                    if (is_null($this->winner)) {
                        $key = 'no_bid';
                    } elseif ($this->getStatus() === self::STATUS_ERROR) {
                        $key = 'has_error';
                    } elseif ($this->hasHistory() && $this->history()->whereNotNull('user_id')->doesntExist()) {
                        $key = 'end_only_bot';
                    } else {
                        $key = 'end';
                    }
                    $status = $isActive ? 'on_site' : 'on_db';
                    $type = $storage_period->get($key)[$status] ?? null;
                    if (!(is_null($type) || is_null($type['date']) || is_null($type['format'])))
                        $exists = !(bool)$this->end->add($type['date'], $type['format'])->diff($current)->invert;
                }
            }
        } catch (Throwable $exception) {
            $exists = true;
            $isActive = $this->hasHistory();
            Log::error("storagePeriodCheck:{$exception->getMessage()}");
        }

        return (object)[
            'exists' => $exists,
            'isActive' => $isActive
        ];
    }
}
