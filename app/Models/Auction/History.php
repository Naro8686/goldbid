<?php

namespace App\Models\Auction;

use App\Events\BetEvent;
use App\Jobs\NextAutoBetJob;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Log;
use Exception;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\Bots\AuctionBot;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use App\Helpers\General\BindsDynamically;
use Throwable;


/**
 * App\Models\Auction\History
 *
 * @method static \Illuminate\Database\Eloquent\Builder|History newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|History newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|History query()
 * @mixin \Eloquent
 * @property-read User $user
 */
class History extends Model
{
    use BindsDynamically;

    protected $primaryKey = "id";
    const COUNT = 1;
    const PREFIX = 'log_';

    protected $fillable = [
        "id",
        "bet",
        "bonus",
        "price",
        "is_bot",
        "bot_num",
        "user_id",
        "nickname",
        "created_at",
        "updated_at",
    ];


    protected $casts = [
        'bet' => 'integer',
        'bot_num' => 'integer',
        'bonus' => 'integer',
        'is_bot' => 'boolean',
        // 'price' => 'float',
    ];
    protected $guarded = ['*'];

    public static function createTable(string $tableName): bool
    {
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id')->index()->nullable();
                $table->unsignedTinyInteger('bet')->default(0)->index();
                $table->unsignedTinyInteger('bonus')->default(0)->index();
                $table->string('price')->unique();
                $table->string('nickname')->index();
                $table->boolean('is_bot')->default(false)->index();
                $table->unsignedTinyInteger('bot_num')->nullable();
                $table->timestamps();
            });
        }
        return true;
    }

    public static function removeTable(string $tableName): bool
    {
        Schema::dropIfExists($tableName);
        return true;
    }

    public static function allTable($like = '%'): Collection
    {
        $operator = is_numeric($like) ? "=" : "LIKE";
        $value = self::PREFIX . $like;
        $all = collect();
        $connectionName = config('database.default', 'mysql');
        $dbName = config("database.connections.$connectionName.database", "goldbid");
        DB::table("INFORMATION_SCHEMA.TABLES")
            ->select("TABLE_NAME")
            ->where("TABLE_TYPE", "BASE TABLE")
            ->where("TABLE_SCHEMA", $dbName)
            ->where("TABLE_NAME", $operator, $value)
            ->orderBy("TABLE_NAME")
            ->chunk(100, function ($tables) use ($connectionName, $all) {
                foreach ($tables as $table) {
                    $history = new History;
                    $tableName = $table->TABLE_NAME;
                    $history->bind($connectionName, $tableName);
                    $auction_id = str_replace(self::PREFIX, '', $tableName);
                    $all->put($auction_id, $history);
                }
            });
        return $all;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function findByAucId(int $id): ?History
    {
        return self::allTable($id)->isNotEmpty() ? self::allTable($id)[$id] : null;
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    function lockTable()
    {
        $connection = $this->getConnectionName();
        $table = $this->getTable();
        if ($connection == 'mysql') {
            $sql = "LOCK TABLES {$table} WRITE";
        }

        if ($connection == 'mssql') {
            $sql = "LOCK TABLE {$table}";
        }

        try {
            if (!empty($sql)) DB::raw($sql);
            else throw new Exception('empty sql');
        } catch (Throwable $e) {
            Log::warning("error from lock table {$table} " . $e->getMessage());
        }
    }

    public function unlockTable()
    {
        DB::raw('UNLOCK TABLES');
    }

    /**
     * @return Auction|Auction[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|\Illuminate\Database\Query\Builder|\Illuminate\Database\Query\Builder[]|mixed|null
     */
    public function auction()
    {
        $result = Str::of($this->getTable())->match('/\d+/');
        return Auction::withTrashed()->find($result->whenEmpty(function () {
            return null;
        }));
    }


    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $isSuccess = true;
            /** @var Auction $auction */
            $auction = $model->auction()->refresh()->fresh(['autoBid', 'bots']);
            try {
                if (!$auction->isFinish()) {
                    if (!$model->is_bot) {
                        $user = $model->user->refresh()->fresh(['autoBid']);
                        $autoBid = $user->autoBid()
                            ->where('auction_id', '=', $auction->id)
                            ->where('count', '>', 0)
                            ->first();
                        DB::transaction(function () use ($model, $auction, $user, $autoBid) {
                            $user->update([
                                'bet' => (int)($user->bet - $model->bet),
                                'bonus' => (int)($user->bonus - $model->bonus),
                            ]);
                            if (!is_null($autoBid)) {
                                $autoBid->count -= 1;
                                if ($autoBid->count <= 0) $autoBid->delete();
                                else $autoBid->save(['timestamp' => false]);
                            }
                            $auction->autoBid()->update(['status' => AuctionBot::PENDING]);
                            $auction->bots()->update(['status' => AuctionBot::PENDING]);
//                            return $auction->refresh()->fresh(['autoBid', 'bots']);
                        });
                    }
                } else throw new Exception("isFinish");
            } catch (Exception|Throwable $exception) {
                $isSuccess = false;
                Log::error('insert history ' . $exception->getMessage());
            }
            return $isSuccess;
        });

        self::created(function ($model) {
            try {
                if ($auction = $model->auction()) {

                    DB::table('auctions')
                        ->where('id', $auction->id)
                        ->update(['step_time' => DB::raw("NOW() + INTERVAL(bid_seconds + 1) SECOND")]);
                    NextAutoBetJob::dispatch($auction);
                    event(new BetEvent($auction));
                }
            } catch (Exception|Throwable $exception) {
                Log::error('created history ' . $exception->getMessage());
            }
        });
    }
}
