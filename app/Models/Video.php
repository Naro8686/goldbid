<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Video
 *
 * @property int $id
 * @property string|null $title
 * @property string $url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Video newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video query()
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereUrl($value)
 * @mixin \Eloquent
 */
class Video extends Model
{
    protected $fillable = ['title', 'url'];

    public function link()
    {
        return (filter_var($this->url, FILTER_VALIDATE_URL) === FALSE) ? asset($this->url) : $this->url;
    }

    public function iframe()
    {
        $link = $this->link();
        if ($url = parse_url($this->url, PHP_URL_HOST)) {
            if (preg_match('/(youtube)/ui', $url, $matches)) {
                if ($matches[0] === "youtube") {
                    parse_str(parse_url($this->url, PHP_URL_QUERY), $query);
                    if (isset($query['v']))
                        $link = "https://www.youtube.com/embed/{$query['v']}";
                }
            }
        }
        return "<iframe src='{$link}' title='{$this->title}' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
    }
}
