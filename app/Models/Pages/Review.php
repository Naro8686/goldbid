<?php

namespace App\Models\Pages;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Pages\Review
 *
 * @property int $id
 * @property string $image
 * @property string|null $alt
 * @property string $title
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property int $published
 * @property-read User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Review wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUserId($value)
 */
class Review extends Model
{
    protected $fillable = ['image', 'alt', 'title', 'description', 'user_id', 'published'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function avatar()
    {
        return $this->image ?? (!is_null($this->user) ? $this->user->avatar() : 'site/img/settings/noavatar.png');
    }
}
