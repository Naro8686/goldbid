<?php

namespace App\Models;

use App\Api\Invoice\Ofd;
use App\Models\Auction\Auction;
use App\Models\Pages\Package;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property string|null $order_num
 * @property string|null $payment_type
 * @property string|null $price
 * @property int $status
 * @property bool $exchanged
 * @property int $user_id
 * @property int $auction_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Auction\Auction $auction
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAuctionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereExchanged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $product_name
 * @property string|null $method
 * @property string $type
 * @property int|null $coupon_id
 * @property-read Package|null $coupon
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereType($value)
 * @property string|null $receipt_id
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereReceiptId($value)
 */
class Order extends Model
{
    const PENDING = 0;
    const SUCCESS = 1;
    const DECLINED = 2;
    const AUCTION = "auction";
    const COUPON = "coupon";
    protected $fillable = ['order_num', 'product_name', 'method', 'type', 'price', 'status', 'exchanged', 'user_id', 'auction_id', 'coupon_id', 'receipt_id'];
    protected $casts = ['status' => 'integer', 'exchanged' => 'boolean'];

    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Package::class);
    }

    public function price(): string
    {
        return Auction::moneyFormat($this->price);
    }

    public function getIdFromPay()
    {
        return $this->method === "card" ? $this->order_num : $this->id;
    }

    public function email(): ?string
    {
        $user = $this->user;
        return !is_null($user) && !empty($user->email) ? $user->email : null;
    }

    public function sendReceipt(): bool
    {
        $ofd = new Ofd(config('ferma.login'), config('ferma.password'));
        $this->receipt_id = $ofd->generateReceipt($this);
        return $this->save();
    }
}
