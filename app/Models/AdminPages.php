<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminPages
 *
 * @property int $id
 * @property string $name
 * @property string|null $icon
 * @property string $route
 * @property string $url
 * @property string|null $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $permissionPages
 * @property-read int|null $permission_pages_count
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminPages whereUrl($value)
 * @mixin \Eloquent
 */
class AdminPages extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'icon', 'route', 'url', 'group'];

    public function icon()
    {
        return $this->icon ?? '';
    }

    public function permissionPages()
    {
        return $this->belongsToMany(Role::class, 'role_admin_pages', 'admin_page_id', 'role_id');
    }
}
