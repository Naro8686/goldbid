<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;

class MessageSentListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param MessageSent $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        if (   is_array($event->data) && array_key_exists('data', $event->data)
            && is_array($event->data['data']) && array_key_exists('upload', $event->data['data'])
            && is_array($event->data['data']['upload']) && array_key_exists('path', $event->data['data']['upload'])
            && file_exists($event->data['data']['upload']['path']) && $image = $event->data['data']['upload']['path']) @unlink($image);
    }
}
