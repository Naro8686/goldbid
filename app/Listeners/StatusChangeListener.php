<?php

namespace App\Listeners;

use App\Events\StatusChangeEvent;
use App\Jobs\StatusChangeJob;
use Illuminate\Contracts\Queue\ShouldQueue;

class StatusChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param StatusChangeEvent $event
     * @return StatusChangeEvent
     */
    public function handle(StatusChangeEvent $event)
    {
        StatusChangeJob::dispatch($event->auction->id)->onQueue('change');
        return $event;
    }
}
