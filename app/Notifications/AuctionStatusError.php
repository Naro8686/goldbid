<?php

namespace App\Notifications;

use App\Models\Auction\Auction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\SmscRu\SmscRuChannel;
use NotificationChannels\SmscRu\SmscRuMessage;

class AuctionStatusError extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Auction
     */
    private $auction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Auction $auction)
    {
        $this->auction = $auction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmscRuChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SmscRuMessage
     */
    public function toSmscRu($notifiable)
    {
        $auctionID = $this->auction->id;
        $link = route('auction.index',$auctionID);
        return SmscRuMessage::create("Аукцион под номером <a href='$link'>&#8470; $auctionID</a> закончился с ошибкой.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
