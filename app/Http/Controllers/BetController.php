<?php

namespace App\Http\Controllers;

use App\Jobs\BidJob;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Auction\Auction;

class BetController extends Controller
{
    public function __invoke($id, Request $request)
    {
        $user = $request->user();
        $auction = Auction::whereStatus(Auction::STATUS_ACTIVE)->findOrFail($id);
        $ordered = $user->auctionOrder()
            ->where('orders.auction_id', '=', $auction->id)
            ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
            ->first('orders.status');
        if (!$auction->finished() && is_null($ordered)) {
            $balance = $user->balance();
            $autobid = $user->autoBid()->where([
                ['auto_bids.auction_id', '=', $auction->id],
                ['auto_bids.count', '>', 0],
            ])->doesntExist();
            if ($autobid && $auction->winner()->nickname !== $user->nickname && ($balance->bet + $balance->bonus) > 0)
                BidJob::dispatchSync($auction, $user->nickname, $user);
        }
    }
}
