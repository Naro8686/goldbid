<?php

namespace App\Http\Controllers;

use App\Api\Payment\Sberbank;
use App\Models\Order;
use App\Models\Video;
use Exception;
use Illuminate\Http\Request;
use App\Mail\FeedbackSendMail;
use App\Models\Auction\Auction;
use App\Models\Pages\Howitwork;
use App\Mail\ReviewSendMail;
use App\Models\Pages\Package;
use App\Models\Pages\Page;
use App\Models\Pages\Question;
use App\Models\Pages\Review;
use App\Settings\ImageTrait;
use App\Settings\SiteSetting;
use App\Models\Pages\Slider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Throwable;


class HomeController extends Controller
{
    use ImageTrait;

    const DIR = 'site.';
    public $page = null;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if (!$request->ajax()) {
            $this->page = (new SiteSetting($request->segment(1)))->page();
            view()->share('page', $this->page);
        }
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $html = $error = null;
            try {
                $auctions = Auction::auctionsForHomePage();
                $html = view('site.include.auctions', ['auctions' => $auctions])->render();
            } catch (Exception|Throwable $e) {
                $error = $e->getMessage();
            }
            return response()
                ->json(['html' => $html, 'error' => $error])
                ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        }
        $banner = SiteSetting::banner() ?? 'site/css/img/bg-slider.png';
        $sliders = Slider::all();
        $ac_content = '';
        $ac_header = '';
        $ac_cont = DB::table('ac_content')->first();
        if (!empty($ac_cont)) {
            $ac_content = $ac_cont->content;
            $ac_header = $ac_cont->header;
        }
        return response()
            ->view(self::DIR . 'index', compact('sliders', 'banner', 'ac_content', 'ac_header'))
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }

    public function howItWorks()
    {
        $steps = Howitwork::all();
        $videos = Video::all();
        $questions = Question::all();
        return view(self::DIR . 'how_it_works', compact('steps', 'questions', 'videos'));
    }

    public function feedback(Request $request)
    {
        if ($request->isMethod('POST')) {
            $request['upload'] = null;
            $request->validate([
                'name' => ['required', 'string', 'max:100'],
                'email' => ['required', 'email', 'max:150'],
                'theme' => ['required', 'integer'],
                'message' => ['required', 'string', 'max:250'],
                'file' => ['sometimes', 'mimes:jpeg,jpg,png,gif,svg,doc,docx,pdf,zip,rar', 'max:2048'],
                'g-recaptcha-response' => config('recaptcha.key') ? ['required', 'recaptcha'] : [],
                'personal_data' => ['required'],
            ]);
            try {
                $request['theme'] = SiteSetting::feedbackTheme($request['theme']);
                if ($request->hasFile('file')) {
                    $request['upload'] = $request['file']->getError() === 0 ? [
                        'path' => public_path($this->uploadImage($request['file'], 'site/img/tmp/feedback', 0, 0, false)),
                        'as' => $request['file']->getClientOriginalName(),
                        'mime' => $request['file']->getClientMimeType(),
                    ] : null;
                }
                if (!is_null(config('mail.from.address'))) {
                    Mail::to(config('mail.from.address'))
                        ->later(now(config('app.timezone'))->addSeconds(5), (new FeedbackSendMail($request->only([
                            'name', 'email', 'theme', 'message', 'upload'
                        ]))));
                }

            } catch (Throwable $exception) {
                Log::error($exception->getMessage());
            }
            return redirect()->back();
        }
        $themes = SiteSetting::feedbackTheme();
        $contact = SiteSetting::siteContacts();

        return view(self::DIR . 'feedback', compact('themes', 'contact'));
    }

    public function reviews(Request $request)
    {
        $user = auth()->user();
        $user_id = (!is_null($user)) ? $user->id : null;

        if ($request->isMethod('POST')) {
            $request['upload'] = null;
            $request->validate([
                'name' => ['required', 'string', 'max:100'],
                'email' => ['required', 'email', 'max:150'],
                'message' => ['required', 'string', 'max:1000'],
                'file' => ['sometimes', 'image', 'mimes:jpeg,jpg,png,gif,svg', 'max:2048'],
                'g-recaptcha-response' => config('recaptcha.key') ? ['required', 'recaptcha'] : [],
                'personal_data' => ['required'],
            ]);
            try {
                if ($request->hasFile('file')) {
                    $request['upload'] = $request['file']->getError() === 0 ? [
                        'path' => public_path($this->uploadImage($request['file'], 'site/img/tmp/review', 200, 100, false)),
                        'as' => $request['file']->getClientOriginalName(),
                        'mime' => $request['file']->getClientMimeType(),
                    ] : null;
                }
                $title = $request['name'];
                $desc = $request['message'];
                if ($user->city) {
                    $title .= " г. $user->city";
                }
                if ($request['upload'] && isset($request['upload']['path'])) {
                    $desc .= "<br><img src='" . asset(str_replace(public_path(), "", $request['upload']['path'])) . "' alt='' style='max-height:100px; max-width:100px'>";
                }

                $user->review()->create([
                    'title' => $title,
                    'description' => $desc,
                ]);
                if (!is_null(config('mail.from.address')))
                    Mail::to(config('mail.from.address'))
                        ->later(now(config('app.timezone'))->addSeconds(5), (new ReviewSendMail($request->only([
                            'name', 'email', 'message', 'upload'
                        ]))));
            } catch (Throwable $exception) {
                Log::error('reviews send file ' . $exception->getMessage());
            }
            return redirect()->back();
        }
        $content = $this->page->meta->content;
        $reviews = Review::where("published", true)
            ->orWhere([
                ["user_id", "<>", null],
                ["published", "<>", true],
                ["user_id", "=", $user_id],
            ])->get();
        return view(self::DIR . 'reviews', compact('reviews', 'content'));
    }

    public function coupon()
    {
        $orderNum = auth()->check() ? SiteSetting::orderNumCoupon(auth()->id()) : null;
        session()->put('orderNumCoupon', $orderNum);
        $packages = Package::where('visibly', true)->get();
        return view(self::DIR . 'coupon', compact('orderNum', 'packages'));
    }


    public function dynamicPage($link = null)
    {
        $dynamicPage = Page::whereHas('footer', function ($query) use ($link) {
            return $query->where('link', '=', $link);
        })->firstOrFail();
        $setting = (new SiteSetting($dynamicPage->slug));
        $page = $setting->content();
        return view(self::DIR . 'dynamic_page', compact('page'));
    }

    public function cookieAgree(Request $request)
    {
        $time = ($agree = (bool)$request['agree']) ? now(config('app.timezone'))->addMonth() : now()->addDay();
        $cookie = cookie('cookiesPolicy', $time, $time->diffInMinutes());
        return response(['agree' => $agree])->cookie($cookie);
    }

    public function fail(Request $request)
    {
        if ($request->has('payment')) {
            $order = Order::whereStatus(Order::PENDING)->findOrFail($request['payment']);
            $order->update(['status' => Order::DECLINED]);
        }

        $msg = $request->has("msg") ? $request["msg"] : "";
        return view('site.order.status', ['status' => 'Ошибка', 'msg' => $msg]);
    }

    public function success(Request $request)
    {
        if ($request->has('payment')) {
            $order = Order::findOrFail($request['payment']);
            if ($order->type === Order::AUCTION) {
                return redirect()->route('profile.orders');
            }
        }
        $msg = $request->has("msg") ? $request["msg"] : "";
        return view('site.order.status', ['status' => 'Спасибо', 'msg' => $msg]);
    }

    public function orderStatus(Request $request)
    {
        try {
            if ($request->has('orderId')) {
                $response = Sberbank::getOrderStatus($request['orderId']);
                switch ($response['status']) {
                    case Order::SUCCESS:
                        return redirect()->route("site.checkout.success", ["msg" => $response["msg"]]);
                    case Order::DECLINED:
                        return redirect()->route("site.checkout.fail", ["msg" => $response["msg"]]);
                }
            }
        } catch (Throwable $throwable) {
            Log::error('orderStatus ' . $throwable->getMessage());
        }
        return view('site.order.status', ['status' => 'Ожидание', 'msg' => 'Ожидайте подтверждение от банка']);
    }

}
