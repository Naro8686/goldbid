<?php

namespace App\Http\Controllers\Api;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class OfdCallback extends Controller
{
    /**
     * @throws Throwable
     */
    public function __invoke(Request $request)
    {
        try {
            $statusCode = 200;
        } catch (Throwable $exception) {
            $statusCode = 500;
            Log::error("OfdCallback: " . $exception->getMessage());
        }
        http_response_code($statusCode);
    }
}
