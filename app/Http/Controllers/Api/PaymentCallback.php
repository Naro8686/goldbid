<?php

namespace App\Http\Controllers\Api;

use App\Api\Payment\PayOk;
use App\Api\Payment\Sberbank;
use DB;
use Exception;
use Throwable;
use App\Models\Order;
use App\Models\Balance;
use App\Models\Mailing;
use Illuminate\Http\Request;
use App\Models\Auction\Step;
use App\Mail\MailingSendMail;
use App\Mail\CouponOrderSendMail;
use App\Mail\AuctionOrderSendMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class PaymentCallback extends Controller
{
    /**
     * @throws Throwable
     */
    public function __invoke($method, Request $request)
    {
        $statusCode = 500;
        $status = Order::PENDING;
        $time = now(config('app.timezone'));
        $orderNumber = null;
        $adminEmail = config('mail.from.address');
        try {
            DB::beginTransaction();
            switch ($method) {
                case Sberbank::NAME:
                    $isCard = ($request->has('status') && $request->has('orderNumber'));
                    $isGPay = $request->has('success');
                    if ($isCard) {
                        $statusCode = 200;
                        $orderNumber = $request['orderNumber'];
                        $sberOperation = $request['operation'];
                        $status = (($sberOperation == 'approved' || $sberOperation == 'deposited'))
                            ? ($request["status"] == 1 ? Order::SUCCESS : Order::PENDING)
                            : Order::DECLINED;
                    } elseif ($isGPay) {
                        $statusCode = $request->input('success') ? 200 : 500;
                        $orderID = $request->input('data.orderId');
                        if (is_null($orderID)) {
                            throw new Exception("GPay - " . $request->has('error.message')
                                ? $request->input('error.message')
                                : "Ошибка: " . json_encode($request->all()));
                        }
                        $data = Sberbank::getOrderStatus($orderID);
                        if ($data && isset($data['orderNumber'])) {
                            $orderNumber = $data['orderNumber'];
                            $status = $data["status"];
                        }
                    }
                    break;
                case PayOk::NAME:
                    $secret = PayOk::getSecKey();
                    $desc = $request->get('desc');
                    $currency = $request->get('currency');
                    $shop = $request->get('shop');
                    $payment_id = $request->get('payment_id');
                    $amount = $request->get('amount');
                    $sign = md5(implode('|', [
                        $secret,
                        $desc,
                        $currency,
                        $shop,
                        $payment_id,
                        $amount
                    ]));

                    if ($sign != $request->get('sign')) {
                        throw new Exception("Подпись не совпадает. payment_id = $payment_id");
                    }
                    if ($product = Order::whereId($payment_id)->where('status', Order::PENDING)->first()) {
                        $statusCode = 200;
                        $orderNumber = $product->order_num;
                        $status = Order::SUCCESS;
                    }
                    break;
            }
            $order = !is_null($orderNumber)
                ? Order::whereOrderNum($orderNumber)
                    ->where('status', Order::PENDING)
                    ->first()
                : null;
            if (is_null($order)) throw new Exception("no Order from DB, method $method");
            $order->update(['status' => $status]);
            $user = $order->user;
            $coupon = $order->coupon;
            $auction = $order->auction;
            if ($order->status === Order::SUCCESS) {
                switch ($order->type) {
                    case Order::COUPON:
                        $user->balanceHistory()
                            ->create([
                                'bonus' => $coupon->bonus,
                                'bet' => $coupon->bet,
                                'reason' => Balance::PURCHASE_BONUS_REASON
                            ]);
                        $referred = $user->referred()->first();
                        if (!is_null($referred) && $user->fullProfile() && $referred->pivot->referral_bonus === 0) {
                            $referralBonus = $coupon->bet / 2;
                            if ($referred->id !== $user->id) {
                                $referred->pivot->update(['referral_bonus' => $referralBonus]);
                                $referred->balanceHistory()
                                    ->create([
                                        'bonus' => $referred->pivot->referral_bonus,
                                        'reason' => Balance::REFERRAL_BONUS_REASON
                                    ]);
                            }
                        }

                        if ($adminEmail) foreach ([$adminEmail] as $key => $email) {
                            Mail::to($email)
                                ->later($time->addSeconds($key * 2), (new CouponOrderSendMail($order)));
                        }
                        break;
                    case Order::AUCTION:
                        if ($auction->type() === Step::BET) {
                            $data = $auction->exchangeBetBonus($user->id);
                            $user->balanceHistory()->create([
                                'type' => Balance::PLUS,
                                'bet' => $data['bet'],
                                'bonus' => $data['bonus'],
                                'reason' => Balance::WIN_REASON,
                            ]);
                        }
                        if ($adminEmail) {
                            Mail::to($adminEmail)
                                ->later($time->addSeconds(2), (new AuctionOrderSendMail($order)));
                            Mail::to($user->email)->later($time->addSeconds(5), (new MailingSendMail(Mailing::CHECKOUT, [
                                "nickname" => $user->nickname,
                                "order_num" => $order->order_num
                            ])));
                        }
                        break;
                    default:
                        throw new Exception("Error Type OrderID: $order->id");
                }
                $order->sendReceipt();
            }
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            $statusCode = 500;
            Log::error("PaymentCallback: " . $exception->getMessage());
        }
        http_response_code($statusCode);
    }
}
