<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Response;
use stdClass;
use Throwable;
use App\Models\Order;
use App\Settings\ImageTrait;
use Illuminate\Http\Request;
use App\Settings\SiteSetting;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class AuctionController extends Controller
{
    use ImageTrait;

    /**
     * @var stdClass
     */
    public $page;


    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if (!$request->ajax()) {
            $this->page = (new SiteSetting($request->segment(1)))->page();
            view()->share('page', $this->page);
        }
    }

    /**
     * @param int $id
     * @return Response
     */
    public function auction(int $id): Response
    {
        $auction = Auction::auctionPage($id);
        return response()
            ->view('site.auction', compact('auction'))
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }


    /**
     * @param int $id
     * @return array|array[]|bool
     */
    public function addFavorite(int $id)
    {
        if ($user_id = Auth::id()) {
            $auction = Auction::findOrFail($id);
            $favorite = $auction->userFavorites();
            return $favorite->toggle([$user_id]);
        }
        return false;
    }

    /**
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws Throwable
     */
    public function autoBid($id, Request $request)
    {
        $auction = Auction::where('status', Auction::STATUS_ACTIVE)->findOrFail($id);
        $user = Auth::user();
        $ordered = $user->auctionOrder()
            ->where('orders.auction_id', '=', $auction->id)
            ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
            ->exists();
        if ($auction->finished() || $ordered) abort(403);
        $time = now(config('app.timezone'))->addSecond();
        $balance = $user->balance();
        $max_count = ($balance->bet + $balance->bonus);
        $request->validate(['count' => ['integer', 'min:0', 'max:' . $max_count, 'nullable']]);
        $count = (int)$request['count'];
        try {
            if ($first = $auction->autoBid()->where('user_id', $user->id)->first()) {
                if ($count === 0) $first->delete();
                else if ((int)$first->count !== $count) $first->update(['count' => $count]);
            } elseif ((bool)$count) $auction->autoBid()
                ->create([
                    'user_id' => $user->id,
                    'count' => ($count),
                    'bid_time' => $time,
                ]);

        } catch (Exception | Throwable $e) {
            Log::error('function autoBid = ' . $e->getMessage());
        }
        if ($request->ajax()) return $count;
        return redirect()->back();
    }

}
