<?php

namespace App\Http\Controllers;

use App\Models\Pages\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SitemapXmlController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $pages = Page::all();
        return response()->view('sitemap', [
            'pages' => $pages
        ])->header('Content-Type', 'text/xml');
    }
}
