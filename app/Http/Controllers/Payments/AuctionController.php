<?php

namespace App\Http\Controllers\Payments;

use App\Api\Payment\Pay;
use Exception;
use Log;
use Throwable;
use App\Models\User;
use App\Models\Order;
use App\Models\Balance;
use Illuminate\View\View;
use App\Models\Auction\Step;
use Illuminate\Http\Request;
use App\Settings\SiteSetting;
use App\Models\Auction\Auction;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Foundation\Application;

class AuctionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'banned', 'order']);
        view()->share(['page' => (new SiteSetting('order'))->page()]);
    }

    public function checkOrder($id): bool
    {
        $user = Auth::user();
        return !$user->auctionOrder()
            ->where('orders.auction_id', '=', $id)
            ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
            ->exists();
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|JsonResponse
     * @throws Throwable
     */
    public function winInfo($id, Request $request)
    {
        $data = [];
        $user = User::findOrFail(Auth::id());
        $auction = Auction::auctionsForHomePageQuery([])->findOrFail($id)->transformAuction($user->id);
        if (!$auction['my_win'] || $auction['error']) abort(403);
        $data['id'] = $auction['id'];
        $data['image'] = $auction['images'][0]['img'];
        $data['alt'] = $auction['images'][0]['alt'];
        $data['title'] = $auction['title'];
        $data['price'] = $auction['price'];
        $data['bet'] = $auction['exchangeBetBonus']['bet'];
        $data['bonus'] = $auction['exchangeBetBonus']['bonus'];
        if ($request["exchange"]) {
            try {
                DB::beginTransaction();
                $run = Order::create([
                    'product_name' => $auction['title'],
                    'status' => Order::SUCCESS,
                    'exchanged' => true,
                    'user_id' => $user->id,
                    'auction_id' => $auction['id']
                ]);
                if ($run) $user->balanceHistory()->create([
                    'type' => Balance::PLUS,
                    'bet' => $data['bet'],
                    'bonus' => $data['bonus'],
                    'reason' => Balance::EXCHANGE_REASON
                ]);
                DB::commit();
            } catch (Throwable $exception) {
                DB::rollBack();
            }
            return redirect()->route('profile.balance');
        } else {
            if ($request->ajax()) {
                try {
                    $modal = view('site.include.winner_modal', compact('data'))->render();
                } catch (Throwable $e) {
                    Log::error($e->getMessage());
                    $modal = "Error";
                }
                return response()->json($modal);
            }
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     * @throws Throwable
     */
    public function order($id, Request $request)
    {
        /** @var Auction $auction */
        /** @var User $user */
        $request->validate([
            'step' => ['required', 'integer', 'between:1,3']
        ]);
        $auction = Auction::whereId($id)->firstOrFail();
        $user = User::where('has_ban', false)->findOrFail(Auth::id());
        $step = (int)$request['step'];
        $winner = ($auction->isFinish() && $auction->winner()->user_id === $user->id);
        $type = $auction->type();
        if (($type !== Step::PRODUCT && !$winner) || ($winner && $auction->status === Auction::STATUS_ERROR)) abort(403, 'Доступ Запрещено!');
        try {
            $price = round(($winner ? $auction->price() : $auction->full_price($user->id)), 1, PHP_ROUND_HALF_UP);
            if (!session()->has("order.$auction->id") || (session()->has("order.$auction->id") && session("order.$auction->id")['price'] !== $price)) {
                session()->forget("order.$auction->id");
                session()->put("order.$auction->id", collect([
                    'order_num' => SiteSetting::orderNumAuction($auction, $user->id),
                    'price' => $price,
                    'exchanged' => false,
                    'product_name' => "$auction->title $auction->short_desc",
                    'auction_id' => $auction->id,
                    'user_id' => $user->id,
                    'type' => Order::AUCTION,
                    'method' => null,
                    'status' => Order::PENDING
                ]));
            }
            $order = session("order.$auction->id");
            switch ($step) {
                case 1:
                    return $this->stepOne($auction, $user, $order, $winner);
                case 2:
                    return $this->stepTwo($auction, $user, $type);
                case 3:
                    return $this->stepThree($auction, $user, $order, $type);
            }
        } catch (Throwable $exception) {
            Log::error($exception->getMessage());
        }
        return redirect()->back()->with('message', 'Error');
    }

    public function stepOne(Auction $auction, User $user, Collection $order, bool $winner, &$data = [])
    {
        $step = Step::where('for_winner', $winner)->where('step', 1)->first();
        $data['text'] = $step->textReplace(['title' => $order['product_name']]);
        $data['title'] = $order['product_name'];
        $data['auction_id'] = $auction->id;
        $data['img'] = $auction->img_1;
        $data['alt'] = $auction->alt_1;
        $data['order_num'] = $order['order_num'];
        $data['winner'] = $winner;
        if (!$winner) {
            $data['full_price'] = $auction::moneyFormat($auction->full_price());
            $data['bid_price'] = $auction::moneyFormat($user->bid_price($auction->id));
            $data['total_price'] = $auction::moneyFormat($auction->full_price($user->id));
        } else {
            $data['auction_price'] = $auction::moneyFormat($auction->price());
        }
        return view('site.order.step_1', compact('data'));
    }

    public function stepTwo(Auction $auction, User $user, $type, &$data = [])
    {
        $data['auction_id'] = $auction->id;
        $data['type'] = $type;
        $data['lname'] = $user->lname;
        $data['fname'] = $user->fname;
        $data['mname'] = $user->mname;
        $step = Step::where('type', $type)->where('step', 2)->first();
        if ($type === Step::PRODUCT) {
            $replace['title'] = $auction->title;
            $data['country'] = $user->country;
            $data['postcode'] = $user->postcode;
            $data['region'] = $user->region;
            $data['city'] = $user->city;
            $data['street'] = $user->street;
            $data['phone'] = User::setPhoneMask($user->phone);
        } elseif ($type === Step::MONEY) {
            $replace['money'] = $auction->short_desc;
            $data['ccnum'] = $user->ccnum;
            $data['payment_type'] = $user->paymentType();
        } else {
            $replace = $auction->exchangeBetBonus($user->id);
        }
        $data['text'] = $step->textReplace($replace);
        return view('site.order.step_2', compact('data'));
    }

    public function stepThree(Auction $auction, User $user, Collection $order, $type, &$data = [])
    {
        $data['lname'] = $user->lname;
        $data['fname'] = $user->fname;
        $data['mname'] = $user->mname;
        if ($type === Step::PRODUCT) {
            $data['country'] = $user->country;
            $data['postcode'] = $user->postcode;
            $data['region'] = $user->region;
            $data['city'] = $user->city;
            $data['street'] = $user->street;
            $data['phone'] = User::setPhoneMask($user->phone);
        } elseif ($type === Step::MONEY) {
            $data['ccnum'] = $user->ccnum;
            $data['payment_type'] = $user->paymentType();
        }
        $validate = array_filter($data, static function ($var) {
            return $var === null;
        });

        if ($type !== Step::BET && count($validate))
            return redirect()->back()->with('message', 'Заполните все необходимые поля в личном кабинете');
        $data['auction_id'] = $auction->id;
        $data['title'] = $auction->title;
        $data['email'] = $user->email;
        $data['order_num'] = $order['order_num'];
        $data['price'] = $auction::moneyFormat($order['price']);
        return view('site.order.step_3', compact('data'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Throwable
     */
    public function buy(Request $request): RedirectResponse
    {
        $user = Auth::user();
        $order = session("order.{$request['id']}");
        $request->validate([
            "id" => ['required', 'exists:auctions,id'],
            "payment_group" => ['required', 'string', function ($attribute, $value, $fail) {
                if (SiteSetting::paymentGroup($value)->isEmpty())
                    $fail('Ошибка!');
            }],
            "order_num" => ['required', function ($attribute, $value, $fail) use ($order) {
                if (is_null($order) || $order['order_num'] !== $value) $fail('error');
            }],
        ]);
        Auction::findOrFail($order['auction_id']);
        $order['method'] = $request['payment_group'];

        if (!$user->email) {
            $request->validate([
                "email" => ['required', 'email', 'unique:users'],
            ]);
            $user->update(['email' => $request['email']]);
        }
        try {
            DB::beginTransaction();
            $order = Order::create($order->all());
            session()->forget("order.$order->auction_id");
            $pay = (new Pay($order))->register();
            $pay_url = $pay->getRedirectUrl();
            if (!$pay_url) throw new Exception();
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('message', 'Что-то пошло не так, попробуйте позже');
        }
        return redirect()->to($pay_url);
    }
}
