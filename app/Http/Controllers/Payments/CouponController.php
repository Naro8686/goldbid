<?php

namespace App\Http\Controllers\Payments;

use App\Api\Payment\Pay;
use App\Api\Payment\PayOk;
use App\Api\Payment\Sberbank;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Pages\Package;
use App\Models\User;
use App\Settings\SiteSetting;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;


class CouponController extends Controller
{
    /**
     * @var User|null $user
     */
    private $user;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            view()->share('user', $this->user);
            return $next($request);
        });
    }

    /**
     * @throws Throwable
     */
    public function buy(Request $request): RedirectResponse
    {
        $request['order_num'] = session('orderNumCoupon');
        $request->validate([
            "coupon_id" => ['required', 'integer', 'exists:packages,id'],
            "payment_group" => ['required', 'string', function ($attribute, $value, $fail) {
                if (SiteSetting::paymentGroup($value)->isEmpty())
                    $fail('Ошибка!');
            }],
            "order_num" => ['required', 'unique:orders'],
        ]);
        $request['method'] = $request['payment_group'];
        $request['user_id'] = $this->user->id;
        $coupon = Package::where('visibly', true)->findOrFail($request['coupon_id']);
        if (!$this->user->email) {
            $request->validate([
                "email" => ['required', 'email', 'unique:users'],
            ]);
            $this->user->update(['email' => $request['email']]);
        }
        $request['product_name'] = "Пакет ставок $coupon->bet";
        $request['type'] = Order::COUPON;
        $request['price'] = $coupon->price;
        session()->forget('orderNumCoupon');
        try {
            DB::beginTransaction();
            $order = Order::create($request->only([
                'user_id', 'coupon_id',
                'order_num', 'type',
                'method', 'product_name',
                'price'
            ]));
            $pay = (new Pay($order))->register();
            $pay_url = $pay->getRedirectUrl();
            if (!$pay_url) throw new Exception();
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('message', 'Что-то пошло не так, попробуйте позже');
        }
        return redirect()->to($pay_url);
    }
}
