<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Auction\Auction;
use App\Models\Auction\Step;
use App\Models\Order;
use App\Settings\ImageTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Log;
use Throwable;

class OrderController extends Controller
{
    use ImageTrait;

    private const DIR = 'admin.orders.';

    public function index(Request $request)
    {
        if ($request->ajax()) {
            try {
                return datatables()->of(Order::selectRaw("*,(price + 0) AS amount")->where('exchanged', false))
                    ->editColumn('method', function ($order) {
                        return ucfirst($order->method);
                    })->editColumn('amount', function ($order) {
                        return Auction::moneyFormat($order->amount,true);
                    })->editColumn('status', function (Order $order) {
                        $class = "btn-outline-info";
                        $status = "В Ожидание";
                        if ($order->status === Order::DECLINED) {
                            $class = "btn-danger";
                            $status = "Отменено";
                        } elseif ($order->status === Order::SUCCESS) {
                            $class = "btn-success";
                            $status = "Успешно";
                        }
                        return "<button class='btn btn-sm btn-block $class'>$status</button>";
                    })->editColumn('created_at', function ($order) {
                        return $order->created_at->format('Y-m-d H:i:s');
                    })
                    ->addColumn('action', function ($order) {
                        $linkDelete = route('admin.orders.destroy', $order['id']);
                        return "<div class='btn-group btn-group-sm' role='group' aria-label='Basic example'>
                                        <button type='button'
                                                class='btn btn-outline-danger'
                                                data-toggle='modal'
                                                data-target='#resourceModal'
                                                data-action='$linkDelete'>
                                                    <i class='fa fa-trash'></i>
                                        </button>
                                    </div>";
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
            } catch (Throwable $exception) {
                Log::error("OrderController: " . $exception->getMessage());
            }
        }
        return view(self::DIR . 'index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $info = 'Шаг ';
        $step = Step::findOrFail($id);
        if ($step->step === 1) {
            $info .= (string)($step->step . ' Для ' . ($step->for_winner ? 'победителя' : 'остальных'));
        } elseif ($step->step === 2) {
            $info .= (string)$step->step;
            if ($step->type === Step::PRODUCT)
                $info .= ' Товар';
            elseif ($step->type === Step::MONEY)
                $info .= ' Деньги';
            elseif ($step->type === Step::BET)
                $info .= ' Ставки';
        }
        return view(self::DIR . 'edit', compact('step', 'info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $step = Step::findOrFail($id);
        $step->update($request->only(['text']));
        return redirect()->route('admin.pages.order')->with('status', 'успешные действия !');
    }

    public function destroy($id)
    {
        $question = Order::findOrFail($id);
        $question->delete();
        return redirect()->route('admin.orders.index')->with('status', 'успешные действия !');
    }
}
