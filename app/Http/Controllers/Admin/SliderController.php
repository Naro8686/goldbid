<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Settings\ImageTrait;
use App\Models\Pages\Slider;
use App\Settings\SiteSetting;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use ImageTrait;

    private const DIR = 'admin.sliders.';

    public function create()
    {
        return view(self::DIR . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => ['required', 'image', 'mimes:jpeg,jpg,png,gif,svg', 'max:2048'],
        ]);
        if ($image = $request->file('file')) $request['image'] = $this->uploadImage($image, 'site/img/settings/sliders');

        Slider::insert($request->only(['image', 'alt']));
        return redirect()->route('admin.pages.home')->with('status', 'успешные действия !');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $slider = Slider::findOrFail($id);
        return view(self::DIR . 'edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'file' => ['sometimes', 'image', 'mimes:jpeg,jpg,png,gif,svg', 'max:2048'],
        ]);
        $slider = Slider::findOrFail($id);
        if ($image = $request->file('file')) $request['image'] = $this->uploadImage($image, 'site/img/settings/sliders');
        if ($request['image'] && is_file(public_path($slider->image))) unlink(public_path($slider->image));
        $slider->update($request->only(['image', 'alt']));
        return redirect()->route('admin.pages.home')->with('status', 'успешные действия !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        if (is_file(public_path($slider->image))) unlink(public_path($slider->image));
        $slider->delete();
        return redirect()->route('admin.pages.home')->with('status', 'успешные действия !');
    }

    public function changeBanner(Request $request): \Illuminate\Http\RedirectResponse
    {
        $banner = SiteSetting::banner();
        if ($request->has('default')) {
            if (!is_null($banner) && is_file(public_path($banner))) unlink(public_path($banner));
            return redirect()->route('admin.pages.home')->with('status', 'Баннер поставлен по умолчанию !');
        }
        $request->validate([
            'banner' => ['required', 'image', 'mimes:jpeg,jpg,png,gif,svg', 'max:2048'],
        ]);
        $this->uploadImage($request->file('banner'), 'site/img/banner');
        if (!is_null($banner) && is_file(public_path($banner))) unlink(public_path($banner));

        return redirect()->route('admin.pages.home')->with('status', 'успешные действия !');
    }
}
