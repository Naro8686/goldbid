<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class VideoController extends Controller
{

    public $extensions = ["mp4", "flv", "m3u8", "3gp", "avi", "wmv", "ogx", "oga", "ogv", "ogg", "webm"];
    private const DIR = 'admin.video.';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view(self::DIR . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $extensions = implode(",",$this->extensions);
        $request->validate([
            //'title' => ['required', 'string', 'max:191'],
            'link' => [Rule::requiredIf(!$request->has('file')), function ($attribute, $val, $fail) use ($request) {
                if (filter_var($val, FILTER_VALIDATE_URL) === FALSE && !$request->has('file')) {
                    $fail('Укажите правильную ссылку!');
                }
            }, 'max:191'],
            'file' => [Rule::requiredIf(!$request->has('link') || trim($request['link']) === ''), 'mimes:'.$extensions, 'max:10000'],
        ]);
        if ($file = $request->file('file')) {
            $path = 'site/img/settings/howitworks/videos';
            $videoName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path($path), $videoName);
            $request['url'] = "$path/$videoName";

        } else {
            $request['url'] = $request['link'];
        }
        Video::create($request->only(['title', 'url']));
        return redirect()->route('admin.pages.howitworks')->with('status', 'успешные действия !');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $video = Video::findOrFail($id);
        return view(self::DIR . 'edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $video = Video::findOrFail($id);
        $extensions = implode(",",$this->extensions);
        $request->validate([
            //'title' => ['required', 'string', 'max:191'],
            'link' => [Rule::requiredIf(!$request->has('file')), function ($attribute, $val, $fail) use ($request, $video) {
                if ($val !== $video->url && filter_var($val, FILTER_VALIDATE_URL) === FALSE && !$request->has('file')) {
                    $fail('Укажите правильную ссылку!');
                }
            }, 'max:191'],
            'file' => [Rule::requiredIf(!$request->has('link') || trim($request['link']) === ''), 'mimes:'.$extensions, 'max:10000'],
        ]);

        if ($file = $request->file('file')) {
            $path = 'site/img/settings/howitworks/videos';
            $videoName = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path($path), $videoName);
            $request['url'] = "$path/$videoName";

        } else {
            $request['url'] = $request['link'];
        }
        if ($request['url'] !== $video->url && is_file(public_path($video->url))) {
            unlink(public_path($video->url));
        }
        $video->update($request->only(['title','url']));
        return redirect()->route('admin.pages.howitworks')->with('status', 'успешные действия !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        if (is_file(public_path($video->url))) unlink(public_path($video->url));
        $video->delete();
        return redirect()->route('admin.pages.howitworks')->with('status', 'успешные действия !');
    }
}
