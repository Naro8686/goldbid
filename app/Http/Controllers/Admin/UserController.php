<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Balance;
use App\Models\User;
use Throwable;

class UserController extends Controller
{
    private const DIR = 'admin.users.';

    public function index(Request $request)
    {
        if ($request->ajax()) {
            try {
                return datatables()->of(User::where('is_admin', false)
                    ->select(['users.*', 'history.participation', 'history.win', 'ref.referrals_count'])
                    ->leftJoinSub("SELECT referred_by, COUNT(referral_id) AS 'referrals_count' FROM referrals GROUP BY referred_by", "ref", "users.id", "=", "ref.referred_by")
                    ->leftJoinSub("SELECT user_id, COUNT(*) AS 'participation', SUM(IF(reason = '" . Balance::WIN_AUCTION . "', 1, 0)) AS 'win' FROM balances WHERE auction_id IS NOT NULL GROUP BY user_id", "history", "users.id", "=", "history.user_id"))
                    ->editColumn('has_ban', function ($user) {
                        $link = route('admin.users.update', $user->id);
                        $class = '';
                        $active = 'false';
                        if ($user->has_ban) {
                            $class = 'active';
                            $active = 'true';
                        }
                        return "<button type='button'
                                class='btn btn-sm btn-toggle {$class}'
                                data-toggle='button'
                                aria-pressed='{$active}'
                                onclick='oNoFF(`{$link}`,{has_ban:($(this).attr(`aria-pressed`) === `true` ? 0 : 1)},`PUT`)'>
                                    <span class='handle'></span>
                                </button>";
                    })->editColumn('participation', function ($user) {
                        return (int)$user->participation;
                    })->editColumn('win', function ($user) {
                        return (int)$user->win;
                    })->editColumn('referrals_count', function ($user) {
                        return (int)$user->referrals_count;
                    })->editColumn('phone', function ($user) {
                        return $user->login();
                    })->editColumn('birthday', function ($user) {
                        return $user->birthday ? $user->birthday->format('Y-m-d') : null;
                    })->editColumn('created_at', function ($user) {
                        return $user->created_at->format('Y-m-d');
                    })->addColumn('action', function ($user) {
                        $linkDelete = route('admin.users.destroy', $user->id);
                        $linkShow = route('admin.users.edit', $user->id);
                        return "<div class='btn-group btn-group-sm' role='group' aria-label='Basic example'>
                                        <button data-href='{$linkShow}' type='button'
                                                data-toggle='modal' data-target='#cardModal'
                                                class='btn btn-info mr-1'><i class='fa fa-eye'></i></button>
                                        <button type='button' class='btn btn-danger' data-toggle='modal'
                                                data-target='#resourceModal'
                                                data-action='{$linkDelete}'>
                                                    удалить
                                        </button>
                                    </div>";
                    })->rawColumns(['has_ban', 'action'])->make(true);
            } catch (Throwable $e) {
            }
        }
        $usersInfo = User::info();
        return view(self::DIR . 'index', compact('usersInfo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws Throwable
     */
    public function edit(int $id)
    {
        if ($user = User::find($id)) {
            $roles = Role::get();
            $data = $user->userCard();
            $html = view('admin.users.card', compact('data', 'roles'))->render();
        } else {
            $html = "<div class='col-md-12'><h3 class='text-center text-danger'>Такого пользователя не существует</h3></div>";
        }
        return response(['success' => true, 'html' => $html, 'title' => 'Карточка пользователя']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $user = User::findOrFail($id);
        $balance = $user->balance();
        if ($request->ajax()) {
            $request->validate([
                'has_ban' => ['required', 'boolean'],
            ]);
            $user->update($request->only('has_ban'));
            $banned = User::info()['banned'];
            return response()->json(['id_name' => 'count_banned', 'change_info' => $banned]);
        }

        $request->validate([
            'role_id' => ['nullable', 'integer', 'exists:roles,id'],
            'bet' => ['sometimes', 'required', 'integer', 'min:0'],
            'bonus' => ['sometimes', 'required', 'integer', 'min:0'],
            'old_bet' => ['sometimes', 'required', 'integer', 'min:0'],
            'old_bonus' => ['sometimes', 'required', 'integer', 'min:0'],
        ]);
        if ((int)$request['old_bet'] !== $balance->bet) {
            $type = Balance::MINUS;
            $bet = abs((int)$request['old_bet'] - $balance->bet);
            if ((int)$request['old_bet'] > $balance->bet) $type = Balance::PLUS;
            $user->balanceHistory()->create([
                'type' => $type,
                'bet' => (int)$bet,
                'reason' => Balance::ADMIN,
            ]);
        }

        if ((int)$request['old_bonus'] !== $balance->bonus) {
            $type = Balance::MINUS;
            $bonus = abs((int)$request['old_bonus'] - $balance->bonus);
            if ((int)$request['old_bonus'] > $balance->bonus) $type = Balance::PLUS;
            $user->balanceHistory()->create([
                'type' => $type,
                'bonus' => (int)$bonus,
                'reason' => Balance::ADMIN,
            ]);
        }

        if ((int)$request['bet'] > 0 || (int)$request['bonus'] > 0) {
            $user->balanceHistory()->create($request->only('bet', 'bonus', 'reason'));
        }
        $user->update(['role_id' => $request->get('role_id')]);
        return redirect()->back()->with('status', 'успешные действия !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        $user = User::findOrFail($id);
        if (is_file(public_path($user->avatar))) unlink(public_path($user->avatar));
        $user->delete();
        return redirect()->back()->with('status', 'успешные действия !');
    }
}
