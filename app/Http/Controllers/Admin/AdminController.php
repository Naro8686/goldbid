<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminPages;
use App\Models\Auction\Auction;
use App\Models\Mailing;
use App\Models\Role;
use App\Models\User;
use App\Rules\OldPasswordRule;
use App\Settings\ImageTrait;
use App\Settings\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Throwable;

class AdminController extends Controller
{
    use ImageTrait;

    const DIR = 'admin.';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function access(Request $request)
    {
        if ($request->method() === "PUT") {
            $request->validate([
                'role_id' => ['required', 'integer', 'exists:roles,id'],
                'admin_page_id' => ['required', 'integer', 'exists:admin_pages,id']
            ]);
            $roleId = $request['role_id'];
            $pageId = $request['admin_page_id'];
            $role = Role::find($roleId);
            $exists = $role->adminPages()->where('id', $pageId)->exists();
            if ($exists) $role->adminPages()->detach([$pageId]);
            else $role->adminPages()->attach([$pageId]);
            return (int)!$exists;
        }
        $roles = Role::get();
        $pages = AdminPages::get();
        return view(self::DIR . 'access', compact('roles', 'pages'));
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function dashboard(Request $request)
    {
        if ($request->ajax()) {
            try {
                if (!Cache::has('auctionData')) Cache::put('auctionData', Auction::data());
                return datatables()->of(Cache::get('auctionData'))->editColumn('img_1', function ($auction) {
                    $img = asset($auction['img_1']);
                    return "<img style='min-width: 70px;max-width: 70px' class='img-fluid img-thumbnail' src='$img' alt='{$auction['alt_1']}'>";
                })->addColumn('action', function ($auction) {
                    $id = $auction['id'];
                    $linkShow = route('admin.auctions.show', $id);
                    $linkDelete = route('admin.auctions.destroy', $id);
                    $linkEditSeo = route('admin.auctions.edit', $id);
                    return "<div class='btn-group btn-group-sm' role='group' aria-label='Basic example'>
                                        <button data-href='$linkShow' type='button'
                                                data-toggle='modal' data-target='#cardModal'
                                                class='btn btn-info mr-1'><i class='fa fa-eye'></i></button>
                                        <a href='$linkEditSeo' class='btn btn-info'>seo</a>

                                        <button type='button' class='btn btn-danger'
                                                data-toggle='modal'
                                                data-target='#resourceModal'
                                                data-action='$linkDelete'>
                                                    удалить
                                        </button>
                                    </div>";
                })->rawColumns(['img_1', 'action'])->make(true);
            } catch (Throwable $e) {
            }
        } else Cache::forget('auctionData');
        $auctionsInfo = Auction::info();
        return view(self::DIR . 'dashboard', compact('auctionsInfo'));
    }

    public function mailConfig(Request $request)
    {
        $mail = SiteSetting::mailConfig();
        if ($request->isMethod('POST')) {
            $request->validate([
                'username' => ['sometimes', 'required', 'string', 'max:50'],
                'password' => ['sometimes', 'required', 'string', 'max:50'],
            ]);
            try {
                Cache::forget('mailConfig');
                if ($request->has('password')) $request['password'] = base64_encode($request['password']);
                $mail->driver = $request['driver'];
                $mail->host = $request['host'];
                $mail->port = $request['port'];
                $mail->from_address = $request['from_address'];
                $mail->from_name = $request['from_name'];
                $mail->encryption = $request['encryption'];
                $mail->username = $request['username'];
                $mail->password = $request['password'];
                $mail->saveOrFail();
                return redirect()->back()->with('status', 'успешные действия !');
            } catch (Throwable $e) {
                return redirect()->back()->with('error', $e->getMessage());
            }
        }
        return view(self::DIR . 'mail_config', compact('mail'));
    }

    public function siteConfig(Request $request)
    {
        $site = SiteSetting::siteConfig();
        if ($request->isMethod('POST')) {
            $request->validate([
                'storage_period' => ['nullable', 'array'],
                'storage_period.no_bid.on_site.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.no_bid.on_db.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.has_error.on_site.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.has_error.on_db.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.end_only_bot.on_site.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.end_only_bot.on_db.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.end.on_site.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'storage_period.end.on_db.date' => ['required_with:storage_period', 'nullable', 'integer', 'min:1'],
                'email' => ['nullable', 'email'],
                'site_enabled' => ['sometimes', 'required', 'boolean'],
                'file' => ['sometimes', 'image', 'mimes:jpeg,jpg,png,gif,svg', 'max:2048'],
            ]);
            if ($image = $request->file('file')) {
                if ($request['image'] = $this->uploadImage($image, 'site/img/settings/maintenance'))
                    if (is_file(public_path($site->image))) unlink(public_path($site->image));
            }
            if ($request->has('site_enabled')) {
                Artisan::call(($request['site_enabled']) ? 'up' : 'down');
            }
            $site->update($request->only(['email', 'phone_number', 'storage_period', 'site_enabled', 'image']));
            return redirect()->back()->with('status', 'успешные действия !');
        }

        return view(self::DIR . 'site_config', compact('site'));
    }

    public function mailing()
    {
        $mailings = collect(['ads' => Mailing::ads(), 'no_ads' => Mailing::no_ads()]);
        return view(self::DIR . 'mailing', compact('mailings'));
    }

    public function adminProfileChange(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = [];
            $request['phone'] = User::unsetPhoneMask($request['phone']);
            if ($request['phone'] !== Auth::user()->phone) {
                $request->validate([
                    'phone' => ['required', 'numeric', 'digits:11', 'unique:users,phone,' . Auth::id()],
                    'current_password' => ['required_with:phone', new OldPasswordRule],
                ]);
                $data['phone'] = $request['phone'];
            }
            if (!is_null($request['new_password'])) {
                $request->validate([
                    'current_password' => ['required_with:new_password', new OldPasswordRule],
                    'new_password' => ['required', 'min:8'],
                    'new_confirm_password' => ['required_with:new_password', 'same:new_password'],
                ]);
                $data['password'] = Hash::make($request['new_password']);
            }
            Auth::user()->update($data);
            return redirect()->back()->with('status', 'Изменения успешно сохранились');
        }
        return view(self::DIR . 'profile');
    }
}
