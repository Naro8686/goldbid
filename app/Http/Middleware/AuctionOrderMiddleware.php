<?php

namespace App\Http\Middleware;

use App\Models\Auction\Auction;
use App\Models\Order;
use App\Models\Auction\Step;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Throwable;

class AuctionOrderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();
        $id = $request['id'] ?? null;
        $error = false;
        $code = 200;
        $message = __('error');
        $auction = Auction::whereId($id)->first();
        if (is_null($user)) {
            $error = true;
            $code = 401;
        } elseif (is_null($id) || is_null($auction)) {
            $error = true;
            $code = 404;
        } elseif (!$auction->type(Step::PRODUCT) && !($auction->status === Auction::STATUS_FINISHED && $auction->winner()->user_id === $user->id)) {
            $error = true;
            $code = 403;
        } elseif ($user->auctionOrder()
            ->where('orders.auction_id', '=', $id)
            ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
            ->exists()) {
            $message = 'Вы уже приобрели этот товар, и больше не можете совершать действия в данном аукционе.';
            $error = true;
        } elseif (!is_null($auction->end) && !(bool)($auction->end->addHours(72)->diff(now($auction->end->timezoneName))->invert)) {
            $error = true;
            $message = 'Аукцион закрыт по истечении отведённого времени';
        }
        if ($error) {
            try {
                if ($request->ajax()) return response()
                    ->json(view('site.include.info_modal', ['message' => $message])->render(), $code)
                    ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                else return redirect()
                    ->route('site.home')
                    ->with('message', $message)
                    ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
            } catch (\Exception | Throwable $e) {
                abort(($code === 200 ? 500 : $code), $message);
            }
        }
        return $next($request);
    }
}
