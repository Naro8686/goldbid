<?php


namespace App\Settings;


use App\Api\Payment\Sberbank;
use App\Models\Auction\Auction;
use App\Models\Auction\Step;
use App\Models\Mail;
use App\Models\Pages\Footer;
use App\Models\Setting as ConfigSite;
use App\Models\Pages\Page;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use stdClass;

class SiteSetting
{
    protected $page;

    /**
     * Setting constructor.
     * @param string|null $slug
     */
    public function __construct(?string $slug)
    {
        $slug = Str::slug($slug);
        $this->page = new stdClass;
        $this->page->footer = new stdClass;
        $this->page->meta = Page::whereSlug($slug)->firstOrNew();
        $this->page->footer->social = Footer::where('show', true)
            ->where('social', true)
            ->orderBy('position')
            ->get();
        $this->page->footer->left = Footer::where('show', true)
            ->where('social', false)
            ->where('float', 'left')
            ->with('page')
            ->orderBy('position')
            ->get();
        $this->page->footer->right = Footer::where('show', true)
            ->where('social', false)
            ->where('float', 'right')
            ->with('page')
            ->orderBy('position')
            ->get();

    }

    public function page(): stdClass
    {
        return $this->page;
    }

    public function meta()
    {
        return $this->page->meta;
    }

    /**
     * @return stdClass
     */
    public function content(): stdClass
    {
        $this->page->content = $this->page->meta->content;
        return $this->page;
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function dynamicURL(string $slug)
    {
        $slug = Str::slug($slug);
        $page = Page::whereSlug($slug)->with('footer')->first();
        return url($page->footer->link);
    }


    public static function siteContacts(): stdClass
    {
        $data = new stdClass();
        $siteConfig = self::siteConfig()->first(['phone_number', 'email']);
        $data->phone = is_null($siteConfig) ? null : $siteConfig->phone_number;
        $data->email = is_null($siteConfig) ? null : $siteConfig->email;
        $data->name = config('app.name') ?? null;
        return $data;
    }

    /**
     * @param int|null $id
     * @return array|mixed
     */
    public static function feedbackTheme(?int $id = null)
    {
        return self::getValue([
            ['id' => 1, 'value' => 'Регистрация'],
            ['id' => 2, 'value' => 'Аккаунт'],
            ['id' => 3, 'value' => 'Баланс'],
            ['id' => 4, 'value' => 'Игровой процесс'],
            ['id' => 5, 'value' => 'Прочее'],
        ], $id);
    }


    /**
     * @param int|null $id
     * @return array[]|mixed|null
     */
    public static function paymentType(?int $id = null)
    {
        return self::getValue([
            ['id' => 1, 'value' => 'Банковская карта'],
            ['id' => 2, 'value' => 'Yandex деньги'],
            ['id' => 3, 'value' => 'Qiwi кошелек'],
        ], $id);
    }

    public static function paymentCoupon(?int $id = null)
    {
        return self::getValue([
            ['id' => 1, 'value' => 'visa', 'short_name' => 'cd', 'img' => asset('site/img/payment/visa.png')],
            ['id' => 2, 'value' => 'mastercard', 'short_name' => 'cd', 'img' => asset('site/img/payment/mastercard.png')],
            ['id' => 3, 'value' => 'mir', 'short_name' => 'cd', 'img' => asset('site/img/payment/Mir-logo.jpg')],
            ['id' => 4, 'value' => Sberbank::NAME, 'short_name' => 'cd', 'img' => asset('site/img/payment/sberbank.jpg')],
            ['id' => 5, 'value' => 'yoo-money', 'short_name' => 'ya', 'img' => asset('site/img/payment/yoo-money.svg')],
            ['id' => 6, 'value' => 'qiwi', 'short_name' => 'qw', 'img' => asset('site/img/payment/qiwi.png')],
            ['id' => 7, 'value' => 'perfect-money', 'short_name' => 'pm', 'img' => asset('site/img/payment/perfect-money.png')],
            ['id' => 8, 'value' => 'mts', 'short_name' => 'mt', 'img' => asset('site/img/payment/mts.png')],
            ['id' => 9, 'value' => 'megafon', 'short_name' => 'mg', 'img' => asset('site/img/payment/megafon.png')],
            ['id' => 10, 'value' => 'beeline', 'short_name' => 'bl', 'img' => asset('site/img/payment/beeline.png')],
            ['id' => 11, 'value' => 'tele2', 'short_name' => 'tl', 'img' => asset('site/img/payment/tele2.png')],
        ], $id);
    }

    /**
     * @param string|null $groupName
     * @return \Illuminate\Support\Collection
     */
    public static function paymentGroup(?string $groupName = null): \Illuminate\Support\Collection
    {
        $group = [];
        foreach (self::paymentCoupon() as $item) {
            if (in_array($item['value'], ['visa', 'mastercard', 'mir', Sberbank::NAME])) {
                $group['card'][] = $item;
            } elseif (in_array($item['value'], ['yoo-money'])) {
                $group['yoo-money'][] = $item;
            } elseif (in_array($item['value'], ['qiwi'])) {
                $group['qiwi'][] = $item;
            } elseif (in_array($item['value'], ['perfect-money'])) {
                $group['perfect-money'][] = $item;
            } elseif (in_array($item['value'], ['mts', 'megafon', 'beeline', 'tele2'])) {
                $group['tel-operators'][] = $item;
            } else {
                $group['others'][] = $item;
            }
        }

        return !is_null($groupName) && isset($group[$groupName]) ? collect($group[$groupName]) : collect(is_null($groupName) ? $group : []);
    }

    /**
     * @param array $payments
     * @param int|null $id
     * @return array|mixed|null
     */
    public static function getValue(array $payments, ?int $id = null)
    {
        if (!is_null($id)) {
            foreach ($payments as $payment) {
                if ($payment['id'] === $id) {
                    $payments = $payment['value'];
                    break;
                }
            }
        }
        return (is_int($id) && is_array($payments)) ? null : $payments;
    }

    public static function orderNumCoupon(int $user_id): string
    {
        $prefix = "ПБ";
        $data_format = now(config('app.timezone'))->format('ymd-His');
        return "$prefix/$user_id/$data_format";
    }

    public static function orderNumAuction(Auction $auction, int $user_id): string
    {
        switch ($auction->type()) {
            case Step::BET:
                $prefix = "ПС";
                break;
            case Step::MONEY:
                $prefix = "СФ";
                break;
            default:
                $prefix = "ТР";
                break;
        }
        $data_format = now(config('app.timezone'))->format('ymd-His');
        return "$prefix-$auction->id/$user_id/$data_format";
    }

    public static function mailConfig()
    {
        return Mail::firstOrNew();
    }

    public static function siteConfig()
    {
        return ConfigSite::first() ?? ConfigSite::create(['phone_number' => '70000000000', 'email' => 'goldbid24@gmail.com']);
    }

    public static function emailRandomCode(): string
    {
        return self::randomNumber(6);
    }

    /**
     * @param $length
     * @return string
     */
    public static function randomNumber($length): string
    {
        $result = '';
        for ($i = 0; $i < $length; $i++) $result .= mt_rand(0, 9);
        return $result;
    }

    public static function timezone()
    {
        try {
            $ip = request()->ip();
            $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
            $ipInfo = json_decode($ipInfo);
            return $ipInfo->timezone;
        } catch (Exception $exception) {
            return config('app.timezone');
        }

    }

    public static function banner()
    {
        $images = File::glob('site/img/banner/*.*');
        return empty($images) || !in_array(pathinfo($images[0], PATHINFO_EXTENSION), ['jpeg', 'jpg', 'png', 'gif', 'svg'])
            ? null
            : $images[0];
    }
}
