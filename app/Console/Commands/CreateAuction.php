<?php

namespace App\Console\Commands;

use App\Jobs\CreateAuctionJob;
use App\Models\Auction\Auction;
use App\Models\Auction\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Throwable;

class CreateAuction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:auctions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Auctions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            Product::where('products.visibly', true)
                ->withCount(['auction' => function ($query) {
                    $query->whereIn('auctions.status', [Auction::STATUS_PENDING, Auction::STATUS_ACTIVE]);
                }])
                ->having('auction_count', '=', 0)
                ->with(['auction'])->chunk(100, function ($products) {
                    foreach ($products as $key => $product) CreateAuctionJob::dispatch($product)
                        ->delay(now(config('app.timezone'))->addSeconds($key * 1));
                });
        } catch (Throwable $throwable) {
            Log::error("CreateAuction: {$throwable->getMessage()}");
        }
    }
}
