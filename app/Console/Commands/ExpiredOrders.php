<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;
use Log;
use Throwable;

class ExpiredOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return bool
     */
    public function handle(): bool
    {
        try {
            $orders = Order::whereStatus(Order::PENDING)
                ->where('updated_at', '<', now(config('app.timezone'))->subHour()->toDateTimeString())
                ->get();
            foreach ($orders as $order) $order->delete();
        } catch (Throwable $e) {
            Log::error($e->getMessage());
        }
        return !empty($orders);
    }
}
