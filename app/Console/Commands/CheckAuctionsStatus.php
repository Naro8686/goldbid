<?php

namespace App\Console\Commands;

use App\Jobs\DeleteAuctionInNotWinner;
use App\Models\Auction\Auction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Throwable;

class CheckAuctionsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:auctions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks the status of completed auctions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            Auction::whereNotNull('end')
                ->chunkById(200, function ($auctions) {
                    foreach ($auctions as $key => $auction) {
                        $storagePeriodCheck = $auction->storagePeriodCheck();
                        $this->info($auction->id . ':' . json_encode($storagePeriodCheck));
                        if ($storagePeriodCheck->exists) ($storagePeriodCheck->isActive)
                            ? $auction->update(['active' => false, 'timestamp' => false])
                            : DeleteAuctionInNotWinner::dispatch($auction, true)->delay(now($auction->end->timezoneName)->addSeconds($key * 1));
                    }
                }, 'id');
        } catch (Throwable $throwable) {
            Log::error("CheckAuctionsStatus: {$throwable->getMessage()}");
        }
    }
}
