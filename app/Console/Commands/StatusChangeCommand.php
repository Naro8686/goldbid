<?php

namespace App\Console\Commands;

use App\Jobs\NextAutoBetJob;
use Cache;
use Exception;
use Psr\SimpleCache\InvalidArgumentException;
use Throwable;
use App\Models\Auction\Auction;
use Illuminate\Console\Command;
use App\Events\StatusChangeEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StatusChangeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changing the status of auctions';

    protected static $run = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public static function clearTransaction()
    {
        try {
            while (DB::transactionLevel() > 0) DB::rollBack();
        } catch (Throwable $e) {
            Log::warning('clearTransaction ' . $e->getMessage());
        }
    }

    public function handle()
    {
        $this->statusChange();
    }

    public function statusChange()
    {
        try {
            DB::table('auctions')->select('id')
                ->where('end', '=', null)
                ->where([
                    ['step_time', '=', null],
                    ['start', '<', DB::raw('NOW()')],
                    ['status', '=', Auction::STATUS_PENDING],
                ])->orWhere([
                    ['step_time', '<>', null],
                    ['step_time', '<', DB::raw('NOW()')],
                    ['status', '=', Auction::STATUS_ACTIVE],
                ])->orderBy('start')->orderBy('step_time')
                ->chunkById(100, function ($auctions) {
                    foreach ($auctions as $value) {
                        $auction = Auction::whereId($value->id)->first();
                        if (!is_null($auction)) switch ($auction->getStatus()) {
                            case Auction::STATUS_PENDING:
                                if ($this->active($auction)) {
                                    event(new StatusChangeEvent($auction));
                                    $this->info("start Auction:$auction->id");
                                }
                                break;
                            case Auction::STATUS_ACTIVE:
                                if ($this->finished($auction)) {
                                    event(new StatusChangeEvent($auction));
                                    $this->info("finished Auction:$auction->id");
                                }
                                break;
                        } else throw new Exception('Not Found');
                    }
                }, 'id');
        } catch (Exception|Throwable $throwable) {
            Log::error('error statusChange ' . $throwable->getMessage());
            $this->clearTransaction();
        }
    }


    /**
     * @param Auction $auction
     * @return bool
     */
    private function active(Auction $auction): bool
    {
        return $auction->update([
            'status' => Auction::STATUS_ACTIVE,
            'step_time' => now(config('app.timezone'))->addSeconds($auction->bid_seconds + 1),
        ]);
    }

    /**
     * @param Auction $auction
     * @return bool
     */
    private function finished(Auction $auction): bool
    {
        $queues = $auction->queues();
        $cacheName = "auction_finished_$auction->id";
        $step_time = is_null($auction->step_time) ? 0 : $auction->step_time->second;

        if (!is_null($queues) && $step_time > 0) {
            if (!Cache::has($cacheName)) Cache::put($cacheName, 1, $step_time);
            else Cache::increment($cacheName);
            if (Cache::get($cacheName) <= 3) {
                NextAutoBetJob::dispatch($auction->refresh()->fresh(['autoBid', 'bots']));
                return false;
            }
        }
        Cache::forget($cacheName);
        $winner = $auction->winner();
        return $auction->update([
            'status' => ((!$auction->shutdownBots() || !$auction->achieved()) && !is_null($winner->nickname) && !$winner->is_bot)
                ? Auction::STATUS_ERROR
                : Auction::STATUS_FINISHED,
            'winner' => (!is_null($winner->nickname) && is_null($auction->winner)) ? $winner : null,
            'end' => now(config('app.timezone')),
            'top' => false,
        ]);
    }
}
