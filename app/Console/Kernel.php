<?php

namespace App\Console;

use App\Console\Commands\CheckAuctionsStatus;
use App\Console\Commands\ExpiredOrders;
use App\Console\Commands\StatusChangeCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\ShortSchedule\ShortSchedule;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        StatusChangeCommand::class,
        CheckAuctionsStatus::class,
        ExpiredOrders::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('check:auctions')
            ->everyFiveMinutes()
            ->withoutOverlapping()
            ->runInBackground();
        $schedule->command('expired:orders')
            ->everyFiveMinutes()
            ->withoutOverlapping()
            ->runInBackground();
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
    }


    protected function shortSchedule(ShortSchedule $shortSchedule)
    {
        $shortSchedule->command('status:change')
            ->everySeconds(0.1)
            ->withoutOverlapping();
        $shortSchedule->command('create:auctions')
            ->everySecond()
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    protected function scheduleTimezone()
    {
        return config('app.timezone');
    }
}
