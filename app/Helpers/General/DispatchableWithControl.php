<?php


namespace App\Helpers\General;


use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

//use App\Models\Automation;
use Illuminate\Foundation\Bus\PendingDispatch;


trait DispatchableWithControl
{
    use \Illuminate\Foundation\Bus\Dispatchable {
        \Illuminate\Foundation\Bus\Dispatchable::dispatch as parentDispatch;
    }

    /**
     * Dispatch the job with the given arguments.
     *
     * @return \Illuminate\Foundation\Bus\PendingDispatch
     */
    public static function dispatch()
    {
        $args = func_get_args();
        if (count($args) < 2) {
            $args[] = Carbon::now(config('app.timezone'));
        }
        list($contactId, $executeAt) = $args;
        $newAutomationArray = [
            'contact_id' => $contactId,
            'job_class_name' => static::class,
            'execute_at' => $executeAt->format('YYYY-MM-DD HH:MI:SS')
        ];
        Log::debug(json_encode($newAutomationArray));
        //Automation::create($newAutomationArray);
        $pendingDispatch = new PendingDispatch(new static(...$args));
        return $pendingDispatch->delay($executeAt);
    }

    /**
     * @param int $contactId
     * @param Carbon $executeAt
     * @return boolean
     */
    public function checkWhetherShouldExecute(int $contactId, Carbon $executeAt): bool
    {
        $conditionsToMatch = [
            'contact_id' => $contactId,
            'job_class_name' => static::class,
            'execute_at' => $executeAt->format('YYYY-MM-DD HH:MI:SS')
        ];
        Log::debug('checkWhetherShouldExecute ' . json_encode($conditionsToMatch));
        //$automation = Automation::where($conditionsToMatch)->first();
        $automation = null;
        if ($automation) {
            $automation->delete();
            Log::debug('checkWhetherShouldExecute = true, so soft-deleted record.');
            return true;
        } else {
            return false;
        }
    }
}
