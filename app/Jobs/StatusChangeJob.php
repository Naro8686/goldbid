<?php

namespace App\Jobs;


use DB;
use Exception;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Throwable;
use App\Models\User;
use App\Models\Balance;
use App\Models\Mailing;
use App\Mail\MailingSendMail;
use Illuminate\Bus\Queueable;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\AuctionStatusError;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;

class StatusChangeJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;
    /**
     * @var int
     */
    private $auctionID;
    private $tag;
    public $uniqueFor = 60;

    /**
     * Create a new job instance.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->auctionID = $id;
        $this->tag = ["auction_status_change:$id"];
    }

    public function tags(): array
    {
        return $this->tag;
    }

    public function uniqueId()
    {
        return $this->auctionID;
    }

    public function middleware()
    {
        return [(new WithoutOverlapping($this->uniqueId()))];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::transaction(function () {
                $auction = Auction::whereId($this->auctionID)->first();
                if ($auction instanceof Auction) {
                    switch ($auction->getStatus()) {
                        case Auction::STATUS_ACTIVE:
                            $this->active($auction);
                            break;
                        case Auction::STATUS_FINISHED:
                            $this->finish($auction);
                            break;
                        case Auction::STATUS_ERROR:
                            $this->error($auction);
                            break;
                    }
                    if ($auction->isFinish() && !empty($auction->winner)) {
                        $this->usersHistorySave($auction);
                        $auction->userFavorites()->detach();
                        $auction->autoBid()->delete();
                    }
                }
            });

//            if ($auction->isFinish() && !is_null($auction->product)) {
//                $product = $auction->product->refresh();
//                CreateAuctionJob::dispatchIf($product->visibly && !$product->activeAuctionExists(), $product);
//            }
        } catch (Exception|Throwable $e) {
            Log::error('StatusChangeJob line = ' . $e->getLine() . ' Message = ' . $e->getMessage());
            $this->delete();
        }
    }

    /**
     * @param Auction $auction
     * @return void
     */
    private function finish(Auction $auction)
    {
        $winner = $auction->winner();
        if (!is_null($winner->nickname) && !$winner->is_bot && $user = User::find($winner->user_id)) {
            if (!is_null(config('mail.from.address')) && $email = $user->email) {
                try {
                    $link = route('auction.index', $auction->id);
                    Mail::to($email)->later(now(config('app.timezone'))->addMinute(), (new MailingSendMail(Mailing::VICTORY, [
                        "nickname" => $winner->nickname,
                        "auction" => "<a style='color: #1B8BCB;' href='$link'>$auction->title $auction->short_desc</a>"
                    ])));
                } catch (Exception|Throwable $e) {
                    Log::error('error send mail to winner ' . $e->getMessage());
                }
            }
        }
    }

    /**
     * @param Auction $auction
     * @return void
     */
    private function active(Auction $auction)
    {
        if ($bot = $auction->botNum(1)) {
            BotBidJob::dispatch($bot, true)
                ->onConnection('bet')
                ->onQueue('auto_bet')
                ->delay(now($auction->start->timezoneName)->addSeconds($bot->timeToBet()));
        }
    }

    /**
     * @param Auction $auction
     * @return void
     */
    private function error(Auction $auction)
    {
        $winner = $auction->winner();
        if ($winner->user_id) {
            $user = User::find($winner->user_id);
            $bets = $auction->bid()->where('user_id', $user->id);
            $user->balanceHistory()->create([
                'reason' => Balance::RETURN_REASON,
                'type' => Balance::PLUS,
                'bet' => $bets->sum('bet'),
                'bonus' => $bets->sum('bonus'),
            ]);
            if ($admin = User::adminProfile()) $admin->notify(new AuctionStatusError($auction));
        }
    }

    private function usersHistorySave(Auction $auction)
    {
        if ($auction->hasHistory()) {
            $auction->history()->whereNotNull('user_id')
                ->groupBy('user_id')->get()
                ->map(function ($history) use ($auction) {
                    $winner = $auction->winner();
                    $user_id = $history->user_id;
                    $bids = $history->where('user_id', $history->user_id);
                    User::find($user_id)->balanceHistory()->create([
                        'reason' => $winner->user_id === $history->user_id ? ($auction->status === Auction::STATUS_ERROR)
                            ? Balance::ERROR_AUCTION : Balance::WIN_AUCTION
                            : Balance::PARTICIPANT_AUCTION,
                        'type' => Balance::MINUS,
                        'bet' => $bids->sum('bet'),
                        'bonus' => $bids->sum('bonus'),
                        'auction_id' => $auction->id,
                    ]);
                });
        }
    }
}
