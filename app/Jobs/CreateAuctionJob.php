<?php

namespace App\Jobs;

use Illuminate\Queue\Middleware\WithoutOverlapping;
use Log;
use Exception;
use Throwable;
use App\Models\Bots\Bot;
use App\Models\Pages\Page;
use App\Models\Bots\BotName;
use App\Settings\ImageTrait;
use Illuminate\Bus\Queueable;
use App\Models\Auction\Auction;
use App\Models\Auction\Product;
use App\Models\Auction\History;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class  CreateAuctionJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ImageTrait;

    public $deleteWhenMissingModels = true;
    /**
     * @var Product
     */
    public $product;
    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public $uniqueFor = 60;
    /**
     * @var array
     */
    private $data = ['img_1' => null, 'img_2' => null, 'img_3' => null, 'img_4' => null];

    /**
     * Create a new job instance.
     *
     * @param Product $product
     */

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * The unique ID of the job.
     *
     * @return int
     */
    public function uniqueId()
    {
        return $this->product->id;
    }

    public function middleware()
    {
        return [(new WithoutOverlapping($this->uniqueId()))];
    }

    /**
     * @throws Throwable
     */
    public function handle()
    {
        try {
            $product = $this->product->refresh();
            $path = 'site/img/auction';
            if (!is_dir(public_path($path))) mkdir(public_path($path), 0777, true);
            $img_1 = $this->imageCopy($product->img_1, 'site/img/product', $path);
            $img_2 = $this->imageCopy($product->img_2, 'site/img/product', $path);
            $img_3 = $this->imageCopy($product->img_3, 'site/img/product', $path);
            $img_4 = $this->imageCopy($product->img_4, 'site/img/product', $path);

            list($min_price, $max_price) = array_pad(str_replace(' ', '', explode('-', $product->bot_shutdown_price)), 2, null);
            $bot_shutdown_price = rand($min_price, $max_price);
            list($min_count, $max_count) = array_pad(str_replace(' ', '', explode('-', $product->bot_shutdown_count)), 2, null);
            $bot_shutdown_count = rand($min_count, $max_count);
            $this->data = [
                'title' => $product->title,
                'short_desc' => $product->short_desc,
                'desc' => $product->desc,
                'specify' => $product->specify,
                'terms' => $product->terms,
                'img_1' => $img_1,
                'img_2' => $img_2,
                'img_3' => $img_3,
                'img_4' => $img_4,
                'alt_1' => $product->alt_1,
                'alt_2' => $product->alt_2,
                'alt_3' => $product->alt_3,
                'alt_4' => $product->alt_4,
                'start_price' => $product->start_price,
                'full_price' => $product->full_price,
                'bot_shutdown_price' => $bot_shutdown_price,
                'bot_shutdown_count' => $bot_shutdown_count,
                'bid_seconds' => $product->step_time,
                'top' => $product->top,
                'step_price' => $product->step_price,
                'start' => now(config('app.timezone'))->addMinutes((int)$product->to_start),
                'exchange' => (bool)$product->exchange,
                'buy_now' => (bool)$product->buy_now,
                'status' => Auction::STATUS_PENDING,
            ];

            if ($auction = $product->auction()->create($this->data)) {
                /** @var $auction Auction */
                $seo = $product->seo();
                $seo['slug'] = $auction->id;

                $this->createBots($auction);
                Page::whereSlug($auction->id)->firstOrCreate($seo);
                History::createTable($auction->getLogTableName());
            } else throw new Exception('auction was not created!');

        } catch (Exception|Throwable $exception) {
            Log::error('CreateAuctionJob ' . $exception->getMessage());
            $images = array_filter([$this->data['img_1'], $this->data['img_2'], $this->data['img_3'], $this->data['img_4']]);
            foreach ($images as $image) $this->deleteImage($image);
        }
    }

    private function createBots(Auction $auction)
    {
        $bots = Bot::where('is_active', true)->get();
        foreach ($bots as $bot) {
            if ($bot->number === 1 && (int)$auction->bot_shutdown_count === 0) continue;
            elseif ($bot->number !== 1 && (int)$auction->bot_shutdown_price === 0) continue;
            $names = $auction->bots()->pluck('name');
            if ($random = BotName::whereNotIn('name', $names)->inRandomOrder()->first()) {
                if (!is_null($bot->change_name)) {
                    list($min, $max) = array_pad(str_replace(' ', '', explode('-', $bot->change_name)), 2, null);
                    $bot->change_name = rand($min, $max);
                }
                if (!is_null($bot->num_moves)) {
                    list($min, $max) = array_pad(str_replace(' ', '', explode('-', $bot->num_moves)), 2, null);
                    $bot->num_moves = rand($min, $max);
                }
                if (!is_null($bot->num_moves_other_bot)) {
                    list($min, $max) = array_pad(str_replace(' ', '', explode('-', $bot->num_moves_other_bot)), 2, null);
                    $bot->num_moves_other_bot = rand($min, $max);
                }
                $auction->bots()->create([
                    'bot_id' => $bot->id,
                    'name' => $random->name,
                    'time_to_bet' => $bot->time_to_bet,
                    'change_name' => $bot->change_name,
                    'num_moves' => $bot->num_moves,
                    'num_moves_other_bot' => $bot->num_moves_other_bot,
                ]);
            } else break;
        }
    }
}
