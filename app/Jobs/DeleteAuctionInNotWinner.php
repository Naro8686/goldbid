<?php

namespace App\Jobs;

use DB;
use Exception;
use Throwable;
use App\Models\Pages\Page;
use App\Settings\ImageTrait;
use Illuminate\Bus\Queueable;
use App\Models\Auction\History;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class DeleteAuctionInNotWinner implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ImageTrait;

    public $deleteWhenMissingModels = true;

    /**
     * @var Auction
     */
    public $auction;
    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public $uniqueFor = 3600;

    /**
     * @var bool
     */
    private $forever;

    public function tags(): array
    {
        return ['delete', 'auction:' . $this->auction->id];
    }

    /**
     * DeleteAuctionInNotWinner constructor.
     * @param Auction $auction
     * @param boolean $forever
     */
    public function __construct(Auction $auction, bool $forever)
    {
        $this->forever = $forever;
        $this->auction = $auction;
    }

    /**
     * The unique ID of the job.
     *
     * @return int
     */
    public function uniqueId()
    {
        return $this->auction->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $history = DB::transaction(function () {
                $history = null;
                $auction = $this->auction->refresh();
                Page::where('slug', $auction->id)->delete();
                $auction->active = !$this->forever;
                $auction->terms =
                $auction->desc =
                $auction->specify =
                $auction->short_desc =
                $auction->alt_1 =
                $auction->alt_2 =
                $auction->alt_3 =
                $auction->alt_4 = null;
                foreach (array_filter([
                    $auction->img_1,
                    $auction->img_2,
                    $auction->img_3,
                    $auction->img_4,
                ]) as $image) $this->deleteImage($image);
                $auction->save(['timestamp' => false]);

                if ($this->forever) {
                    $history = $auction->getLogTableName();
                    $auction->forceDelete();
                } else $auction->delete();

                return $history;
            });
            if (!is_null($history)) History::removeTable($history);
        } catch (Exception|Throwable $e) {
            Log::error('delete_auction_job ' . $e->getMessage());
        }
    }
}
