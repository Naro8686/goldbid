<?php

namespace App\Jobs;

use Exception;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Throwable;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use App\Models\Auction\AutoBid;
use App\Models\Auction\History;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutoBidJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;

    public $tries = 1;
    /**
     * AutoBidJob constructor.
     * @param AutoBid $autoBid
     */
    public $autoBid;

    private $currentName;
    private $tag;
    public $uniqueFor = 1;

    public function __construct(AutoBid $autoBid)
    {
        $this->tag = ["auto_bet:{$autoBid->auction_id}", "NickName:{$autoBid->user->nickname}"];
        $this->autoBid = $autoBid;
        $this->currentName = $this->autoBid->auction->winner()->nickname;
    }

    public function uniqueId()
    {
        return "{$this->autoBid->auction_id}:{$this->autoBid->user_id}";
    }

    public function middleware()
    {
        return [(new WithoutOverlapping($this->uniqueId()))->dontRelease()];
    }

    public function failed(Throwable $throwable)
    {
        $this->delete();
        if ($this->autoBid && $this->autoBid->auction) {
            $auction = $this->autoBid->auction->refresh()->fresh(['autoBid', 'bots']);
            $auction->bots()->update(['status' => AutoBid::PENDING]);
            $auction->autoBid()->update(['status' => AutoBid::PENDING]);
            if ($throwable->getCode() !== 666 && $nextJob = $auction->queues()) {
                if ($model = $nextJob['model']->refresh()) {
                    $user = $botNum = null;
                    if ($model instanceof AutoBid) $user = $model->user;
                    else $botNum = $model->number();
                    BidJob::dispatchSync($auction, $nextJob['nickname'], $user, $botNum);
                }
                Log::warning('AutoBidJob ' . $throwable->getMessage());
            }
        }
    }

    public function tags(): array
    {
        return $this->tag;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Throwable
     */
    public function handle()
    {
        try {
            $autoBid = $this->autoBid->refresh();
            $auction = $autoBid->auction->refresh()->fresh(['autoBid', 'bots']);

            if ($auction->isFinish()) {
                throw new Exception('Auction End', 666);
            }

            if ($this->currentName !== $auction->winner()->nickname && $auction->jobExists()) {
                throw new Exception('delete old job', 666);
            }

            $user = $autoBid->user->refresh();
            $balance = $user->balance();
            $ordered = $user->auctionOrder()
                ->where('orders.auction_id', '=', $autoBid->auction_id)
                ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
                ->exists();
            if ($ordered || (($balance->bet + $balance->bonus) < History::COUNT) || $autoBid->count < 1) {
                throw new Exception('fail');
            } else BidJob::dispatchSync($auction->refresh(), $user->nickname, $user);
        } catch (Exception|Throwable $exception) {
            $this->failed($exception);
        }
    }

}
