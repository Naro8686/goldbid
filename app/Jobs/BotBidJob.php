<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Throwable;
use Exception;
use Illuminate\Bus\Queueable;
use App\Models\Bots\AuctionBot;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;

class BotBidJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;

    public $tries = 1;

    /**
     * @var AuctionBot
     */
    public $auctionBot;
    /**
     * @var bool
     */
    private $first;

    private $tag;
    /**
     * @var \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed
     */
    private $currentName;
    public $uniqueFor = 1;

    /**
     * BotBidJob constructor.
     * @param AuctionBot $auctionBot
     * @param bool $first
     */
    public function __construct(AuctionBot $auctionBot, bool $first = false)
    {
        $this->tag = ["auto_bet:{$auctionBot->auction_id}", "NickName:{$auctionBot->name}"];
        $this->auctionBot = $auctionBot;
        $this->first = $first;
        $this->currentName = $this->auctionBot->auction->winner()->nickname;
    }

    public function uniqueId()
    {
        return "{$this->auctionBot->auction_id}:{$this->auctionBot->bot_id}";
    }

    public function middleware()
    {
        return [(new WithoutOverlapping($this->uniqueId()))->dontRelease()];
    }

    public function failed(Throwable $throwable)
    {
        $this->delete();
        if ($this->auctionBot && $this->auctionBot->auction) {
            $auction = $this->auctionBot->auction->refresh()->fresh(['autoBid', 'bots']);
            $auction->bots()->update(['status' => AuctionBot::PENDING]);
            $auction->autoBid()->update(['status' => AuctionBot::PENDING]);
            if ($throwable->getCode() !== 666 && $nextJob = $auction->queues()) {
                if ($model = $nextJob['model']->refresh()) {
                    $user = $botNum = null;
                    if ($model instanceof AuctionBot) $botNum = $model->number();
                    else $user = $model->user;
                    BidJob::dispatchSync($auction, $nextJob['nickname'], $user, $botNum);
                }
                Log::warning('BotBidJob ' . $throwable->getMessage());
            }
        }
    }

    public function tags(): array
    {
        return $this->tag;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Throwable
     */
    public function handle()
    {
        if ($auctionBot = $this->auctionBot->refresh()) {
            try {
                $auction = $auctionBot->auction->refresh()->fresh(['autoBid', 'bots']);

                if ($this->currentName !== $auction->winner()->nickname && $auction->jobExists()) {
                    throw new Exception('delete old job', 666);
                }

                if ($auction->isFinish()) {
                    throw new Exception('Auction End', 666);
                }

                if ($this->first && $auction->bid()->take(1)->exists()) {
                    $auctionBot->delete();
                    throw new Exception('ignored first', 666);
                }

                if ($this->action($auctionBot)) {
                    BidJob::dispatchSync($auction->refresh(), $auctionBot->name, null, $auctionBot->number());
                } else throw new Exception("fail $auctionBot->name");
            } catch (Exception|Throwable $exception) {
                $this->failed($exception);
            }
        }
    }

    /**
     * @param AuctionBot $auctionBot
     * @return bool
     */
    public function action(AuctionBot $auctionBot): bool
    {
        switch ($auctionBot->number()) {
            case 1:
                return $this->botOne($auctionBot);
            case 2:
            case 3:
                return $this->botTwoThree($auctionBot);
            default:
                return false;
        }
    }

    /**
     * @param AuctionBot $auctionBot
     * @return bool
     */
    public function botOne(AuctionBot $auctionBot): bool
    {
        $run = false;
        try {
            if ((int)$auctionBot->change_name <= 0) $auctionBot = $auctionBot->botRefresh();
            if (!is_null($auctionBot) && !is_null($auctionBot->auction) && (int)$auctionBot->change_name > 0) {
                $auctionBot->change_name -= 1;
                $run = $auctionBot->save();
            }
        } catch (Exception|Throwable $e) {
            $this->failed($e);
        }
        return ($run !== false);
    }

    /**
     * @param AuctionBot $auctionBot
     * @return bool
     */
    public function botTwoThree(AuctionBot $auctionBot): bool
    {
        $run = false;
        try {
            if (((int)$auctionBot->num_moves + (int)$auctionBot->num_moves_other_bot) <= 0) $auctionBot = $auctionBot->botRefresh();
            if (!is_null($auctionBot)) {
                $key = ($auctionBot->num_moves > 0 ? "num_moves" : "num_moves_other_bot");
                if ($auctionBot->$key > 0) {
                    $auctionBot->$key -= 1;
                    $run = $auctionBot->save();
                }
            }
        } catch (Exception|Throwable $e) {
            $this->failed($e);
        }
        return ($run !== false);
    }
}
