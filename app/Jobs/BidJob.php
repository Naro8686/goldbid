<?php

namespace App\Jobs;

use App\Models\Auction\AutoBid;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Log;
use Throwable;
use Exception;
use App\Models\User;
use App\Events\BetEvent;
use Illuminate\Bus\Queueable;
use App\Models\Order;
use App\Models\Auction\Auction;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\Middleware\WithoutOverlapping;

class BidJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const BID_COUNT = 1;
    /**
     * @var string
     */
    public $bid_type = 'bet';
    /**
     * @var User|null
     */
    public $user;
    /**
     * @var string
     */
    public $nickname;
    /**
     * @var Auction
     */
    public $auction;
    /**
     * @var int|null
     */
    private $botNum;
    /**
     * @var bool
     */
    private $is_bot;
    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public $uniqueFor = 1;
    public $start;

    /**
     * BidJob constructor.
     * @param Auction $auction
     * @param string $nickname
     * @param User|null $user
     * @param int|null $botNum
     */
    public function __construct(Auction $auction, string $nickname, User $user = null, ?int $botNum = null)
    {
        $this->user = $user;
        $this->auction = $auction;
        $this->nickname = $nickname;
        $this->botNum = $botNum;
        $this->is_bot = !is_null($botNum);
        $this->start = microtime(true);
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return "{$this->auction->id}:$this->nickname";
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware()
    {
        return [(new WithoutOverlapping($this->uniqueId()))->dontRelease()];
    }

    /**
     * @param $auction
     * @return bool
     */
    private function validation($auction): bool
    {
        $checkBalance = self::BID_COUNT;
        $ordered = false;
        if (!$this->is_bot) {
            $user = $this->user->refresh();
            $balance = $user->balance();
            $ordered = $user->auctionOrder()
                ->where('orders.auction_id', '=', $auction->id)
                ->whereIn('orders.status', [Order::SUCCESS, Order::PENDING])
                ->exists();
            $checkBalance = (int)($balance->bet + $balance->bonus);
            $this->bid_type = $balance->bet > 0 ? 'bet' : 'bonus';
        }
        return (
            !is_null($auction) &&
            !$ordered &&
            $checkBalance >= self::BID_COUNT &&
            $auction->winner()->nickname !== $this->nickname &&
            !$auction->isFinish()
        );
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Throwable
     */
    public function handle()
    {
        $auction = $this->auction->refresh()->fresh(['autoBid', 'bots']);
        try {
            if ($this->validation($auction)) {
                $data = [
                    $this->bid_type => self::BID_COUNT,
                    'price' => $auction->new_price(),
                    'nickname' => $this->nickname,
                    'is_bot' => $this->is_bot,
                ];
                if (!$this->is_bot) $data['user_id'] = $this->user->id;
                else $data['bot_num'] = $this->botNum;
                if (!$auction->history()->create($data)) {
                    throw new Exception("$this->nickname failed");
                }
            } else throw new Exception("$this->nickname validation");
        } catch (Throwable $exception) {
            if ($auction) {
                if ($this->is_bot || $auction->autoBid()->where('user_id', $this->user->id)->exists()) {
                    $auction->autoBid()->update(['status' => AutoBid::PENDING]);
                    $auction->bots()->update(['status' => AutoBid::PENDING]);
                }
                NextAutoBetJob::dispatch($auction
                    ->refresh()
                    ->fresh(['autoBid', 'bots']))
                    ->onConnection('sync');
            }
            Log::error($exception->getMessage());
        }
    }
}
