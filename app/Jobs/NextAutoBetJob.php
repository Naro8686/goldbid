<?php

namespace App\Jobs;

use App\Models\Auction\Auction;
use App\Models\Auction\AutoBid;
use App\Models\Bots\AuctionBot;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Throwable;

class NextAutoBetJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Auction
     */
    private $auction;
    public $start;
    public $uniqueFor = 1;

    public function uniqueId()
    {
        return $this->auction->id;
    }

    /**
     * Create a new job instance.
     *
     * @param Auction $auction
     */
    public function __construct(Auction $auction)
    {
        $this->start = microtime(true);
        $this->auction = $auction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $auction = $this->auction->refresh()->fresh(['autoBid', 'bots']);
            Cache::forget("auction.queues.$auction->id");
            $now = now(config('app.timezone'));
            $job = $auction->queues();
            if (!$auction->isFinish() && !is_null($job)) {
                $model = $job['model'];
                $job['className']::dispatchIf($model->update(['status' => AutoBid::WORKED]), $model)
                    ->onConnection('bet')
                    ->onQueue('auto_bet')
                    ->delay($now->addSeconds($model->timeToBet((microtime(true) - $this->start))));
            }
        } catch (Throwable|Exception $throwable) {
            Log::error($throwable->getMessage());
            $this->failed($throwable);
        }
    }

    public function failed(Throwable $throwable)
    {
        $this->delete();
        $auction = $this->auction;
        $auction->autoBid()->update(['status' => AutoBid::PENDING]);
        $auction->bots()->update(['status' => AuctionBot::PENDING]);
        Log::error('BetListener ' . $throwable->getMessage() . ' line ' . $throwable->getLine());
    }
}
