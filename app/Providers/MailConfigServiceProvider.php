<?php

namespace App\Providers;

use App\Models\Mail;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $mail = Cache::rememberForever('mailConfig', function () {
            return (Schema::hasTable('mails'))
                ? Mail::firstOrNew()
                : new Mail();
        });
        if ($mail) {
            $config = [
                'driver' => $mail->driver ?? 'smtp',
                'host' => $mail->host ?? 'smtp.mailgun.org',
                'port' => $mail->port ?? 465,
                'from' => [
                    'address' => $mail->from_address,
                    'name' => $mail->from_name
                ],
                'encryption' => $mail->encryption ?? 'tls',
                'username' => $mail->username,
                'password' => $mail->getPassword(),
                'sendmail' => '/usr/sbin/sendmail -bs',
                'pretend' => false,
            ];
            Config::set('mail', $config);
        }
    }
}
